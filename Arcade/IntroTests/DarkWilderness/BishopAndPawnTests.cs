using Intro.DarkWilderness;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DarkWilderness;

public class BishopAndPawnTests : BaseTest<BishopAndPawn, BishopAndPawn.Input, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(BishopAndPawn.Input input) => Solution.solution(input);

}
