using Intro.RainsOfReason;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainsOfReason;

public class VariableNameTests : BaseTest<VariableName, string, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(string input) => Solution.solution(input);

}
