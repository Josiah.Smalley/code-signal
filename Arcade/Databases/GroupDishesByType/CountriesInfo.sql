-- setup
CREATE TABLE countries
(
    name       varchar(255),
    continent  varchar(255),
    population int
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT COUNT(*) AS number, AVG(population) AS average, SUM(population) AS total FROM countries;
END;