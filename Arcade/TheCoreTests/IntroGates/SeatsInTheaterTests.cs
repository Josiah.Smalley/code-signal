using NUnit.Framework;
using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates;

public class SeatsInTheaterTests : BaseTest<SeatsInTheater, SeatsInTheater.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(SeatsInTheater.Input input) => Solution.solution(input);


}
