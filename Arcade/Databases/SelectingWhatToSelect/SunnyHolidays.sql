-- setup
CREATE TABLE holidays (
    holiday_date DATE
);

CREATE TABLE weather (
    sunny_date DATE
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT sunny_date as ski_date FROM weather
        INNER JOIN holidays ON sunny_date=holiday_date
    ORDER BY sunny_date;
END