using System;
using System.Linq;
using Common;

namespace Intro.DarkWilderness;

public class KnapsackLight : IHaveSolution<KnapsackLight.Input, int> {

    public class Input {

        public int Value1 { get; set; }
        public int Weight1 { get; set; }
        public int Value2 { get; set; }
        public int Weight2 { get; set; }
        public int MaxW { get; set; }

    }

    public int solution(Input input) =>
        solution(input.Value1,
            input.Weight1,
            input.Value2,
            input.Weight2,
            input.MaxW
        );

    public int solution(
        int value1,
        int weight1,
        int value2,
        int weight2,
        int maxW
    ) {
        var options = new Tuple<int, int>[] {
            new(0, 0), // nothing
            new(value1, weight1), // just the first
            new(value2, weight2), // just the second
            new(value1 + value2, weight1 + weight2), // both
        };

        return options.OrderBy(tup => tup.Item1) // sort by value
            .Last(tup => tup.Item2 <= maxW) // find the most valuable option with weight <= maxW
            .Item1; // return the value of that item
    }


}
