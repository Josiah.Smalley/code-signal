CREATE PROCEDURE solution()
BEGIN
    SELECT TABLE_NAME  AS tab_name,
           COLUMN_NAME AS col_name,
           DATA_TYPE   AS data_type
    FROM information_schema.columns COLS
    WHERE TABLE_SCHEMA = 'ri_db'
      AND TABLE_NAME LIKE BINARY 'E%S'
    ORDER BY tab_name, col_name;
END