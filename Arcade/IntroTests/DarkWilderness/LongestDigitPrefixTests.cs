using Intro.DarkWilderness;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DarkWilderness; 

public class LongestDigitPrefixTests: BaseTest<LongestDigitPrefix, string, string> {

    [TestCaseSource(typeof(DataSource))]
    public override string TestsFromFile(string input) => Solution.solution(input);

}
