-- setup
CREATE TABLE users
(
    first_name  varchar(255),
    second_name varchar(255),
    attribute   varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT *
    FROM users
    WHERE attribute like BINARY CONCAT('_%\%', first_name, '\_', second_name, '\%%')
    ORDER BY attribute;
END;