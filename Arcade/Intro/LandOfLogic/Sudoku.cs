using System.Collections.Generic;
using System.Linq;
using Common;

namespace Intro.LandOfLogic; 

public class Sudoku: IHaveSolution<int[][], bool> {

    public bool solution(int[][] grid) {
        return grid.All(row => row.ToHashSet().Count == 9) && // check rows
            Enumerable.Range(0, 9)
                .Select(col => grid.Select(row => row[col]))
                .All(column => column.ToHashSet().Count == 9) && // check cols
            squaresValid(grid);

    }

    private bool squaresValid(int[][] grid) {
        for (var rowStart = 0; rowStart < 9; rowStart += 3) {
            for (var colStart = 0; colStart < 9; colStart += 3) {
                var values = new HashSet<int>();

                for (var row = 0; row < 3; row++) {
                    for (var col = 0; col < 3; col++) {
                        values.Add(grid[rowStart + row][colStart + col]);
                    }
                }

                if (values.Count != 9) {
                    return false;
                }
            }
        }

        return true;
    }

}
