-- setup
CREATE TABLE events
(
    event_id INT,
    date     DATE,
    user_id  INT
);

CREATE TABLE settings
(
    user_id   INT,
    timeshift INT,
    hours     INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT event_id,
           DATE_FORMAT(DATE_ADD(date, INTERVAL timeshift MINUTE),
                       IF(hours = 12, '%Y-%m-%d %h:%i %p', '%Y-%m-%d %H:%i')) AS formatted_date
    FROM events
             INNER JOIN settings ON events.user_id = settings.user_id
    ORDER BY event_id;
END