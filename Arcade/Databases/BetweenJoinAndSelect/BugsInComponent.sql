-- setup
CREATE TABLE Bug
(
    num   INT,
    title VARCHAR(255)
);

CREATE TABLE Component
(
    id    INT,
    title VARCHAR(255)
);

CREATE TABLE BugComponent
(
    bug_num      INT,
    component_id INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT DISTINCT Bug.title                                              bug_title,
                    Component.title                                        component_title,
                    (SELECT COUNT(*)
                     FROM BugComponent BC
                     WHERE BugComponent.component_id = BC.component_id) AS bugs_in_component
    FROM BugComponent
             INNER JOIN Bug ON Bug.num = BugComponent.bug_num
             INNER JOIN Component ON Component.id = BugComponent.component_id
    WHERE (SELECT COUNT(*) FROM BugComponent BC3 WHERE BC3.bug_num = BugComponent.bug_num) > 1
    ORDER BY bugs_in_component DESC, component_id, bug_num;
END;