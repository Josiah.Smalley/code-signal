using System.Linq;

namespace Intro.IslandOfKnowledge;

public class IsIPv4Address {

    public bool solution(string inputString) {
        var pieces = inputString.Split('.');

        return pieces.Length == 4 &&
            pieces

                // Check the string length first to short-circuit the need for expensive parsing
                .All(piece => IsValidLength(piece) &&
                    !HasPaddedZeros(piece) &&
                    int.TryParse(piece, out var pieceAsInt) &&
                    IsInRange(pieceAsInt)
                );
    }

    private static bool IsValidLength(string piece) => piece.Length is > 0 and < 4;

    private static bool HasPaddedZeros(string piece) => piece.Length > 1 && piece[0] == '0';

    private static bool IsInRange(int pieceAsInt) => pieceAsInt is >= 0 and < 256;

}
