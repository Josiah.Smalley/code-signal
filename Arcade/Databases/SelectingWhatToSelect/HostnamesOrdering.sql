-- setup
CREATE TABLE hostnames
(
    id       INT,
    hostname VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Segments AS (SELECT id,
                             hostname,
                             LENGTH(hostname) - LENGTH(REPLACE(hostname, '.', '')) + 1 AS numSegments
                      FROM hostnames),
         Exploded AS (SELECT id,
                             hostname,
                             IF(numSegments < 3, NULL, SUBSTRING_INDEX(hostname, '.', 1))    AS firstHostname,
                             IF(numSegments < 2, NULL,
                                SUBSTRING_INDEX(SUBSTRING_INDEX(hostname, '.', -2), '.', 1)) AS secondHostname,
                             SUBSTRING_INDEX(hostname, '.', -1)                              AS thirdHostname
                      FROM Segments)
    SELECT id, hostname
    FROM Exploded
    ORDER BY thirdHostname, secondHostname, firstHostname;
END;