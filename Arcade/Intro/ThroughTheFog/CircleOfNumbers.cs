using Common;

namespace Intro.ThroughTheFog;

public class CircleOfNumbers : IHaveSolution<CircleOfNumbers.Input, int> {

    public class Input {

        public int n { get; set; }
        public int firstNumber { get; set; }

    }

    public int solution(Input input1) => solution(input1.n, input1.firstNumber);

    public int solution(int n, int firstNumber) => (firstNumber + (n / 2)) % n;


}
