CREATE TABLE Grades
(
    Name     varchar(255),
    ID       int,
    Midterm1 int,
    Midterm2 int,
    Final    int
);

CREATE PROCEDURE solution()
BEGIN
    SELECT NAME, ID
    FROM Grades
    WHERE 2 * Final > Midterm1 + Midterm2 -- Both inequalities reduce to this same formula
    ORDER BY SUBSTR(Name, 1, 3), ID;
END;