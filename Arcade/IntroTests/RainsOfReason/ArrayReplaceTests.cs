using Intro.RainsOfReason;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainsOfReason;

public class ArrayReplaceTests : BaseTest<ArrayReplace, ArrayReplaceTests.InputData, int[]> {

    public class InputData {

        public int[] InputArray { get; set; }
        public int ElemToReplace { get; set; }
        public int SubstitutionElem { get; set; }

    }

    [TestCaseSource(typeof(DataSource))]
    public override int[] TestsFromFile(InputData input) =>
        Solution.solution(input.InputArray, input.ElemToReplace, input.SubstitutionElem);

}
