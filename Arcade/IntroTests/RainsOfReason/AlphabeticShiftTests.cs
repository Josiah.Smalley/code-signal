using Intro.RainsOfReason;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainsOfReason; 

public class AlphabeticShiftTests: BaseTest<AlphabeticShift,string,string> {

    [TestCaseSource(typeof(DataSource))]
    public override string TestsFromFile(string input) => Solution.solution(input);

}
