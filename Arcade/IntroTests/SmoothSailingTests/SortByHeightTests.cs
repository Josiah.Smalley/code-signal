using Intro.SmoothSailing;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.SmoothSailingTests; 

public class SortByHeightTests: BaseTest<SortByHeight> {

    [Test]
    public void Test1() {
        var input = new[]{-1, 150, 190, 170, -1, -1, 160, 180};
        var expected = new[]{ -1, 150, 160, 170, -1, -1, 180, 190 };

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test2() {
        var input = new[]{-1, -1, -1, -1, -1};
        var expected = new[]{-1, -1, -1, -1, -1};

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test3() {
        var input = new[]{-1};
        var expected = new[]{-1};

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test4() {
        var input = new[]{4, 2, 9, 11, 2, 16};
        var expected = new[]{2, 2, 4, 9, 11, 16};

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test5() {
        var input = new[]{2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1};
        var expected = new[]{1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 2};

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test6() {
        var input = new[]{23, 54, -1, 43, 1, -1, -1, 77, -1, -1, -1, 3};
        var expected = new[]{1, 3, -1, 23, 43, -1, -1, 54, -1, -1, -1, 77};

        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }

}
