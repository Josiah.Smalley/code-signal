-- setup
CREATE TABLE full_year
(
    id         int,
    newspaper  varchar(255),
    subscriber varchar(255)
);
CREATE TABLE half_year
(
    id         int,
    newspaper  varchar(255),
    subscriber varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT DISTINCT subscriber
    FROM (SELECT subscriber
          from full_year
          WHERE newspaper LIKE '%daily%'
          UNION
          SELECT subscriber
          from half_year
          WHERE newspaper like '%daily%') unioned
    ORDER BY subscriber;
END