-- setup
CREATE TABLE scores
(
    arbiter_id       INT,
    first_criterion  INT,
    second_criterion INT,
    third_criterion  INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Extrema AS (SELECT MIN(first_criterion)  AS min1,
                            MAX(first_criterion)  AS max1,
                            MIN(second_criterion) AS min2,
                            MAX(second_criterion) AS max2,
                            MIN(third_criterion)  AS min3,
                            MAX(third_criterion)  AS max3
                     FROM scores)
    SELECT scores.*
    FROM scores,
         Extrema
    WHERE (IF(first_criterion IN (min1, max1), 1, 0) + IF(second_criterion IN (min2, max2), 1, 0) +
           IF(third_criterion IN (min3, max3), 1, 0) < 2)
    ORDER BY arbiter_id;

END;