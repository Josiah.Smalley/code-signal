using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic; 

public class SudokuTests : BaseTest<Sudoku, int[][], bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(int[][] input) => Solution.solution(input);

    

}
