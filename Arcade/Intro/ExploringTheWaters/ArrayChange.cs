namespace Intro.ExploringTheWaters;

public class ArrayChange {

    public int solution(int[] inputArray) {
        var moves = 0;

        for (var i = 1; i < inputArray.Length; i++) {
            if (inputArray[i] > inputArray[i - 1]) {
                continue;
            }

            var diff = inputArray[i - 1] - inputArray[i] + 1;
            moves += diff;
            inputArray[i] = inputArray[i - 1] + 1;

        }

        return moves;
    }


}
