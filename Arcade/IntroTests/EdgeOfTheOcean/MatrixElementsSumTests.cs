using Intro.EdgeOfTheOcean;
using Newtonsoft.Json;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EdgeOfTheOcean;

public class MatrixElementsSumTests : BaseTest<MatrixElementsSum> {


    [Test]
    public void Test1() {
        var matrix = FromString<int[][]>(@"[[0,1,1,2], 
                [0,5,0,0], 
            [2,0,3,3]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 9);
    }

    [Test]
    public void Test2() {
        var matrix = FromString<int[][]>(@"[[1,1,1,0], 
            [0,5,0,1], 
            [2,1,3,10]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 9);
    }

    [Test]
    public void Test3() {
        var matrix = FromString<int[][]>(@"[[1,1,1], 
            [2,2,2], 
            [3,3,3]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 18);
    }

    [Test]
    public void Test4() {
        var matrix = FromString<int[][]>(@"[[0]]");

        Assert.IsTrue(Solution.solution(matrix) == 0);
    }

    [Test]
    public void Test5() {
        var matrix = FromString<int[][]>(@"[[1,0,3], 
 [0,2,1], 
 [1,2,0]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 5);
    }

    [Test]
    public void Test6() {
        var matrix = FromString<int[][]>(@"[[1], 
 [5], 
 [0], 
 [2]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 6);
    }

    [Test]
    public void Test7() {
        var matrix = FromString<int[][]>(@"[[1,2,3,4,5]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 15);
    }

    [Test]
    public void Test8() {
        var matrix = FromString<int[][]>(@"[[2], 
 [5], 
 [10]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 17);
    }

    [Test]
    public void Test9() {
        var matrix = FromString<int[][]>(@"[[4,0,1], 
 [10,7,0], 
 [0,0,0], 
 [9,1,2]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 15);
    }

    [Test]
    public void Test10() {
        var matrix = FromString<int[][]>(@"[[1]]"
        );

        Assert.IsTrue(Solution.solution(matrix) == 1);
    }
    
    

}
