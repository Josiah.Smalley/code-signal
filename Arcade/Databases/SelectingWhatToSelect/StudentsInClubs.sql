CREATE PROCEDURE solution()
SELECT * FROM students
WHERE EXISTS (
    SELECT C.id FROM clubs C WHERE C.id = students.club_id
)
ORDER BY students.id;
