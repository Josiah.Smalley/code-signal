-- setup
CREATE TABLE Successors
(
    name     VARCHAR(255),
    birthday DATE,
    gender   CHAR
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT CONCAT(IF(gender = 'M', 'King', 'Queen'), ' ', name) AS name FROM Successors ORDER BY birthday;
END