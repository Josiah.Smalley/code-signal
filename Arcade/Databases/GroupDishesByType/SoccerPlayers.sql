-- setup
CREATE TABLE soccer_team
(
    id            int,
    first_name    varchar(255),
    surname       varchar(255),
    player_number int
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT GROUP_CONCAT(CONCAT(first_name, ' ', surname, ' #', player_number) ORDER BY player_number SEPARATOR '; ') players
    FROM soccer_team;
END