-- setup
CREATE TABLE moviesInfo
(
    title    varchar(255),
    director varchar(255),
    year     int,
    oscars   int
);


-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT director
    FROM moviesInfo
    WHERE year > 2000
    GROUP BY director
    HAVING SUM(oscars) > 2
    ORDER BY director;
END