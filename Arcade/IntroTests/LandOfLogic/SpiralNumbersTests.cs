using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic; 

public class SpiralNumbersTests : BaseTest<SpiralNumbers, int, int[][]> {

    [TestCaseSource(typeof(DataSource))]
    public override int[][] TestsFromFile(int input) => Solution.solution(input);

    

}
