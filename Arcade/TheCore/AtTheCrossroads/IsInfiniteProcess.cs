using Common;

namespace TheCore.AtTheCrossroads;

public class IsInfiniteProcess : IHaveSolution<IsInfiniteProcess.IsInfiniteProcessInput, bool> {

    public class IsInfiniteProcessInput {

        public int a { get; set; }
        public int b { get; set; }

    }

    public bool solution(IsInfiniteProcessInput input) => solution(input.a, input.b);

    public bool solution(int a, int b) => a > b || ((a % 2) != (b % 2));


}
