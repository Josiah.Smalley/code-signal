-- setup
CREATE TABLE departments
(
    dep_name VARCHAR(255)
);

CREATE TABLE employees
(
    emp_name VARCHAR(255)
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT dep_name, emp_name
    FROM departments,
         employees
    ORDER BY dep_name, emp_name;
END