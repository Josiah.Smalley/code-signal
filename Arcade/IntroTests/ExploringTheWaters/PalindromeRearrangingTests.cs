using Intro.ExploringTheWaters;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ExploringTheWaters; 

public class PalindromeRearrangingTests: BaseTest<PalindromeRearranging> {

    [Test]
    public void Test1() {
        Assert.IsTrue(Solution.solution("aabb"));
    }
    
    [Test]
    public void Test2() {
        Assert.IsFalse(Solution.solution("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaabc"));
    }
    
    [Test]
    public void Test3() {
        Assert.IsTrue(Solution.solution("abbcabb"));
    }
    
    [Test]
    public void Test4() {
        Assert.IsTrue(Solution.solution("zyyzzzzz"));
    }
    
    [Test]
    public void Test5() {
        Assert.IsTrue(Solution.solution("z"));
    }
    
    [Test]
    public void Test6() {
        Assert.IsTrue(Solution.solution("zaa"));
    }
    
    [Test]
    public void Test7() {
        Assert.IsFalse(Solution.solution("abca"));
    }
    
    [Test]
    public void Test8() {
        Assert.IsFalse(Solution.solution("abcad"));
    }
    
    [Test]
    public void Test9() {
        Assert.IsFalse(Solution.solution("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbccccaaaaaaaaaaaaa"));
    }
    
    [Test]
    public void Test10() {
        Assert.IsFalse(Solution.solution("abdhuierf"));
    }

}
