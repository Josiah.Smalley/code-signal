using NUnit.Framework;
using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates;

public class PhoneCallTests : BaseTest<PhoneCall, PhoneCall.PhoneCallInput, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(PhoneCall.PhoneCallInput input) => Solution.solution(input);


}
