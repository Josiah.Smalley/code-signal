using NUnit.Framework;
using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates; 

public class LateRideTests : BaseTest<LateRide, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);

    

}
