using System.Linq;
using Common;

namespace Intro.RainbowOfClarity;

public class DeleteDigit : IHaveSolution<int, int> {

    public int solution(int input) {
        var inputString = input.ToString();

        return Enumerable.Range(0, inputString.Length)
            .Select(i => int.Parse(inputString.Remove(i, 1)))
            .Max();
    }

}
