using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class ExtraNumberTests : BaseTest<ExtraNumber, ExtraNumber.ExtraNumberInput, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(ExtraNumber.ExtraNumberInput input) => Solution.solution(input);

    

}
