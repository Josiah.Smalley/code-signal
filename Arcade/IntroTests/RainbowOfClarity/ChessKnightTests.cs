using Intro.RainbowOfClarity;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainbowOfClarity; 

public class ChessKnightTests : BaseTest<ChessKnight, string, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(string input) => Solution.solution(input);

    

}
