using Intro.DivingDeeper;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DivingDeeper; 

public class ArrayMaxConsecutiveSumTests: BaseTest<ArrayMaxConsecutiveSum, ArrayMaxConsecutiveSum.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(ArrayMaxConsecutiveSum.Input input) => Solution.solution(input);

}
