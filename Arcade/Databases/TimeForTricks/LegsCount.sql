-- setup
CREATE TABLE creatures
(
    id   int,
    name varchar(255),
    type enum ('human', 'cat', 'dog')
);

-- solution
DROP PROCEDURE IF EXISTS solution;
CREATE PROCEDURE solution()
SELECT SUM(IF(type = 'human', 2, 4)) as summary_legs
FROM creatures
ORDER BY id;
