-- setup
CREATE TABLE expressions
(
    id        int,
    a         int,
    b         int,
    operation varchar(1),
    c         int
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT *
    FROM expressions
    WHERE c = CASE
                  WHEN operation = '+' THEN a + b
                  WHEN operation = '-' THEN a - b
                  WHEN operation = '*' THEN a * b
                  ELSE a / b
        END
    ORDER BY id;
END;