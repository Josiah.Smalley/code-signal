using Intro.EdgeOfTheOcean;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EdgeOfTheOcean;

public class MakeArrayConsecutive2Tests : BaseTest<MakeArrayConsecutive2>
{
    [Test]
    public void Test1()
    {
        Assert.AreEqual(3, Solve(6, 2, 3, 8));
    }
    [Test]
    public void Test2()
    {
        Assert.AreEqual(2, Solve(0, 3));
    }
    [Test]
    public void Test3()
    {
        Assert.AreEqual(0, Solve(5, 4, 6));
    }
    [Test]
    public void Test4()
    {
        Assert.AreEqual(3, Solve(6, 2, 3, 8));
    }
    [Test]
    public void Test5()
    {
        Assert.AreEqual(0, Solve(1));
    }

    private int Solve(params int[] statues) => Solution.solution(statues);
}