using Common;

namespace TheCore.IntroGates;

public class MaxMultiple : IHaveSolution<MaxMultiple.MaxMultipleInput, int> {

    public class MaxMultipleInput {

        public int divisor { get; set; }
        public int bound { get; set; }

    }

    public int solution(MaxMultipleInput input) => solution(input.divisor, input.bound);

    public int solution(int divisor, int bound) => divisor * (bound / divisor);


}
