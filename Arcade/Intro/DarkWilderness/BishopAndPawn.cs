using System;
using Common;

namespace Intro.DarkWilderness; 

public class BishopAndPawn: IHaveSolution<BishopAndPawn.Input, bool> {

    public class Input {

        public string Bishop { get; set; }
        public string Pawn { get; set; }

    }

    public bool solution(Input input) => solution(input.Bishop, input.Pawn);

    public bool solution(string bishop, string pawn) => Math.Abs(bishop[0] - pawn[0]) == Math.Abs(bishop[1] - pawn[1]);


}
