using Intro.ThroughTheFog;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ThroughTheFog; 

public class AbsoluteValuesSumMinimizationTests: BaseTest<AbsoluteValuesSumMinimization, int[], int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int[] input) => Solution.solution(input);

}
