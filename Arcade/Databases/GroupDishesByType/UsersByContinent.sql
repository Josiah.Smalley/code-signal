-- setup
CREATE TABLE countries
(
    country   varchar(255),
    continent varchar(255),
    users     int
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT continent, SUM(users) as users
    FROM countries
    GROUP BY continent
    ORDER BY users DESC;
END;