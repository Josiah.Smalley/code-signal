DROP FUNCTION IF EXISTS response;
CREATE FUNCTION response(name VARCHAR(40)) RETURNS VARCHAR(200) DETERMINISTIC
BEGIN
    -- first: extract
    SET @first := SUBSTRING_INDEX(name, ' ', 1);
    SET @last := SUBSTRING_INDEX(name, ' ', -1);

    -- second: format
    SET @first := CONCAT(UPPER(SUBSTR(@first, 1, 1)), LOWER(SUBSTR(@first, 2)));
    SET @last := CONCAT(UPPER(SUBSTR(@last, 1, 1)), LOWER(SUBSTR(@last, 2)));

    -- finally: create the message
    RETURN CONCAT('Dear ', @first, ' ', @last, '! We received your message and will process it as soon as possible. Thanks for using our service. FooBar On! - FooBarIO team.');
END;

CREATE PROCEDURE solution()
BEGIN
    SELECT id, name, response(name) AS response
    FROM customers;
END;
