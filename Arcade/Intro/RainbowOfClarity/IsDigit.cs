using Common;

namespace Intro.RainbowOfClarity; 

public class IsDigit: IHaveSolution<char, bool> {

    public bool solution(char input) => input is >= '0' and <= '9';

}
