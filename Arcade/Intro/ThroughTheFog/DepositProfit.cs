using System;
using Common;

namespace Intro.ThroughTheFog; 

public class DepositProfit: IHaveSolution<DepositProfit.Input, int> {

    public class Input {

        public int Deposit { get; set; }
        public int Rate { get; set; }
        public int Threshold { get; set; }

    }

    public int solution(Input input1) => solution(input1.Deposit, input1.Rate, input1.Threshold);

    public int solution(int deposit, int rate, int threshold) {
        if (deposit >= threshold) {
            return 0;
        }
        
        var percentage = 1 + (rate / 100.0);

        return (int)Math.Ceiling(Math.Log(((double)threshold) / deposit, percentage));
    }

}
