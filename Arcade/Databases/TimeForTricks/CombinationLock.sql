-- setup
CREATE TABLE discs
(
    id         int,
    characters varchar(255),
    color      varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT ROUND(EXP(SUM(LN(LENGTH(characters)))), 0) FROM discs;
END