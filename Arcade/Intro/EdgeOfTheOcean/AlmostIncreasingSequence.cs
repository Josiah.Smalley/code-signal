using System;

namespace Intro.EdgeOfTheOcean;

public class AlmostIncreasingSequence
{
public bool solution(int[] sequence)
{
    return sequence.Length < 3 || CanCreateSequence(sequence);
}

private bool CanCreateSequence(int[] sequence, int start = 0, bool canRemove = true)
{
    if (start >= sequence.Length - 1)
    {
        return true;
    }

    if (sequence[start] < sequence[start + 1])
    {
        return CanCreateSequence(sequence, start + 1, canRemove);
    }

    if (!canRemove)
    {
        return false; // can't remove a second one
    }

    var canRemoveStart = start == 0 || sequence[start - 1] < sequence[start + 1];
    if (canRemoveStart && CanCreateSequence(sequence, start + 1, false))
    {
        return true;
    }

    var canRemoveNext = start + 2 >= sequence.Length || sequence[start] < sequence[start + 2];
    return canRemoveNext && CanCreateSequence(sequence, start + 2, false);
}
}