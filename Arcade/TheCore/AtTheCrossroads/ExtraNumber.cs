using Common;

namespace TheCore.AtTheCrossroads;

public class ExtraNumber : IHaveSolution<ExtraNumber.ExtraNumberInput, int> {

    public class ExtraNumberInput {

        public int a { get; set; }
        public int b { get; set; }
        public int c { get; set; }

    }

    public int solution(ExtraNumberInput input) => solution(input.a, input.b, input.c);

    public int solution(int a, int b, int c) => a ^ b ^ c;


}
