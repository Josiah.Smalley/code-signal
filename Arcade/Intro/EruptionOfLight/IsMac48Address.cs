using System.Linq;
using Common;

namespace Intro.EruptionOfLight;

public class IsMac48Address : IHaveSolution<string, bool> {

    public bool solution(string address) {
        var segments = address.Split("-");

        return segments.Length == 6 &&
            segments.All(segment => segment.Length == 2 &&
                segment.All(c => c is >= '0' and <= '9' or >= 'A' and <= 'F')
            );
    }

}
