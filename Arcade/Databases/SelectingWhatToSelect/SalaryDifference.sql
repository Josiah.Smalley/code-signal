-- setup
CREATE TABLE employees
(
    id     INT,
    name   VARCHAR(255),
    salary INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Extrema AS (SELECT COALESCE(MAX(salary), 0) AS max_sal, COALESCE(MIN(salary), 0) AS min_sal FROM employees)
    SELECT max_sal * (SELECT COUNT(*) FROM employees WHERE salary = max_sal)
               - min_sal * (SELECT COUNT(*) FROM employees WHERE salary = min_sal) AS salary_diff
    FROM Extrema;
END;