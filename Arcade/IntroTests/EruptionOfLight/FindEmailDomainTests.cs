using Intro.EruptionOfLight;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EruptionOfLight;

public class FindEmailDomainTests : BaseTest<FindEmailDomain, string, string> {

    [TestCaseSource(typeof(DataSource))]
    public override string TestsFromFile(string input) => Solution.solution(input);

}
