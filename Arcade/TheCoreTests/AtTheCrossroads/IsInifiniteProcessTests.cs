using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class IsInifiniteProcessTests : BaseTest<IsInfiniteProcess, IsInfiniteProcess.IsInfiniteProcessInput, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(IsInfiniteProcess.IsInfiniteProcessInput input) => Solution.solution(input);

    

}
