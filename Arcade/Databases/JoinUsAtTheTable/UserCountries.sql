-- setup
CREATE TABLE users
(
    id   INT,
    city VARCHAR(255)
);

CREATE TABLE cities
(
    city    VARCHAR(255),
    country VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT id,
           IFNULL(country, 'unknown') AS country
    FROM users U
             LEFT JOIN cities C ON U.city = C.city
    ORDER BY U.id;
END