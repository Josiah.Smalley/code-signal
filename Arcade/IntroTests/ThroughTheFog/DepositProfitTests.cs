using Intro.ThroughTheFog;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ThroughTheFog;

public class DepositProfitTests : BaseTest<DepositProfit, DepositProfit.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(DepositProfit.Input input) => Solution.solution(input);

}
