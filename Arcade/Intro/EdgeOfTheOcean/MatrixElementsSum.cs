using System.Linq;

namespace Intro.EdgeOfTheOcean;

public class MatrixElementsSum {

    public int solution(int[][] matrix) => linqSolution(matrix);

    private static int linqSolution(int[][] matrix) {
        return Enumerable.Range(0, matrix[0].Length)
            .Select(col =>
                Enumerable.Range(0, matrix.Length).TakeWhile(row => matrix[row][col] != 0).Sum(row => matrix[row][col])
            )
            .Sum();
    }

    private static int naiveSolution(int[][] matrix) {
        var numRows = matrix.Length;
        var numCols = matrix[0].Length;

        var sum = 0;

        for (var col = 0; col < numCols; col++) {
            for (var row = 0; row < numRows; row++) {
                if (matrix[row][col] == 0) {
                    break;
                }

                sum += matrix[row][col];
            }
        }

        return sum;
    }

}
