using System.Linq;

namespace Intro.SmoothSailing; 

public class AllLongestStrings {

    public string[] solution(string[] inputArray) {
        var maxLength = inputArray.Max(x => x.Length);
        return inputArray.Where(x => x.Length == maxLength).ToArray();
    }

}
