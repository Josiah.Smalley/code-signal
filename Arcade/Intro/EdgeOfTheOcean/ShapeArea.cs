namespace Intro.EdgeOfTheOcean;

public class ShapeArea
{
    public int solution(int n)
    {
        return 2 * n * (n - 1) + 1;
    }
}