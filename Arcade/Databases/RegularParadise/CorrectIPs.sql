-- setup
CREATE TABLE ips
(
    id INT,
    ip VARCHAR(255)
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT *
    FROM ips
    WHERE ip REGEXP '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$'
    AND (
        ip REGEXP '\.[1-9][0-9]$' OR
        ip REGEXP '\.[1-9][0-9]\.[^\.]+$'
        )
    ORDER BY id;
END