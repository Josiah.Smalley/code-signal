using System.Linq;
using Common;

namespace Intro.LandOfLogic;

public class DifferentSquares : IHaveSolution<int[][], int> {

    public int solution(int[][] matrix) =>
        Enumerable.Range(0, matrix.Length-1)
            .SelectMany(row =>
                Enumerable.Range(0, matrix[0].Length-1)
                    .Select(col => generateCode(matrix, row, col))
            )
            .ToHashSet()
            .Count;

    private string generateCode(int[][] matrix, int row, int col) =>
        $"{matrix[row][col]}{matrix[row][col + 1]}{matrix[row + 1][col]}{matrix[row + 1][col + 1]}";

}
