DROP FUNCTION IF EXISTS get_total;
CREATE FUNCTION get_total(items VARCHAR(45)) RETURNS INT DETERMINISTIC
BEGIN
    SET @total := 0;

    WHILE LENGTH(items) <> 0
        DO

            SET @nextId := SUBSTRING_INDEX(items, ';', 1);

            SET @nextPrice := (SELECT price FROM item_prices WHERE id = @nextId);

            SET @total := @total + @nextPrice;

            SET @nextSeparator = LOCATE(';', items);
            IF @nextSeparator = 0 THEN
                SET items := '';
            ELSE
                SET items := SUBSTR(items, @nextSeparator + 1);
            END IF;

        END WHILE;

    RETURN @total;
END;

CREATE PROCEDURE solution()
BEGIN
    SELECT id, buyer, get_total(items) AS total_price
    FROM orders
    ORDER BY id;
END;
