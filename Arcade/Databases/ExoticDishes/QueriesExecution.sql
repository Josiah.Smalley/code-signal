-- setup
CREATE TABLE queries(
    query_name VARCHAR(255),
    code VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SET @q := (
        SELECT GROUP_CONCAT(CONCAT('SELECT "', query_name, '", (', code, ') val') SEPARATOR ' UNION ')
        FROM queries
        ORDER BY query_name
    );

    PREPARE stmt FROM @q;
    EXECUTE stmt;
END;