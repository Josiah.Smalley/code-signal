-- setup
CREATE TABLE inheritance
(
    derived VARCHAR(255),
    base    VARCHAR(255)
);

CREATE TABLE variables
(
    var_name VARCHAR(255),
    type     VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SET SESSION cte_max_recursion_depth=10000;
    WITH RECURSIVE Roots AS (SELECT var_name, type
                             FROM variables
                             UNION
                             SELECT R.var_name, base
                             FROM Roots R
                                      INNER JOIN inheritance ON R.type = inheritance.derived
                             WHERE inheritance.derived != 'Number')
    SELECT DISTINCT V.var_name, V.type
    FROM variables V
             INNER JOIN Roots ON Roots.var_name = V.var_name
    WHERE Roots.type = 'Number'
      AND V.type != 'Number';
END;