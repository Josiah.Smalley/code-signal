using Common;

namespace Intro.DarkWilderness; 

public class DigitDegree: IHaveSolution<int, int> {

    public int solution(int input) {
        if (input < 10) {
            return 0;
        }

        var sum = 0;

        while (input > 0) {
            sum += input % 10;
            input /= 10;
        }

        return 1 + solution(sum);
    }

}
