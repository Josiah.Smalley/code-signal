-- setup
CREATE TABLE departments
(
    id          int,
    name        varchar(255),
    description varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT COUNT(*) as number_of_nulls
    FROM departments
    WHERE description IS NULL
       OR description REGEXP '^ *(null|nil|-) *$';
END;