using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge;

public class BoxBlurTests : BaseTest<BoxBlur, int[][], int[][]> {

    [TestCaseSource(typeof(DataSource))]
    public override int[][] TestsFromFile(int[][] input) => Solution.solution(input);

}
