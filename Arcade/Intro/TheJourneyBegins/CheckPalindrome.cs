namespace Intro.TheJourneyBegins {
    public class CheckPalindrome {

        public bool solution(string s) {
            for (int i=0, j=s.Length-1; i<j; i++, j--) {
                if (s[i] != s[j]) {
                    return false;
                }
            }

            return true;
        }

    }
}
