using Intro.EdgeOfTheOcean;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EdgeOfTheOcean;

public class AlmostIncreasingSequenceTests : BaseTest<AlmostIncreasingSequence>
{
    [Test]
    public void Test1()
    {
        Assert.IsFalse(Solve(1, 3, 2, 1));
    }
    
    [Test]
    public void Test2()
    {
        Assert.IsTrue(Solve(1, 3, 2));
    }
    
    [Test]
    public void Test3()
    {
        Assert.IsFalse(Solve(1, 2, 1, 2));
    }
    
    [Test]
    public void Test4()
    {
        Assert.IsFalse(Solve(3, 6, 5, 8, 10, 20, 15));
    }
    
    [Test]
    public void Test5()
    {
        Assert.IsFalse(Solve(1, 1, 2, 3, 4, 4));
    }
    
    [Test]
    public void Test6()
    {
        Assert.IsFalse(Solve(1, 4, 10, 4, 2));
    }
    
    [Test]
    public void Test7()
    {
        Assert.IsTrue(Solve(10, 1, 2, 3, 4, 5));
    }
    
    [Test]
    public void Test8()
    {
        Assert.IsFalse(Solve(1, 1, 1, 2, 3));
    }
    
    [Test]
    public void Test9()
    {
        Assert.IsTrue(Solve(0, -2, 5, 6));
    }
    
    [Test]
    public void Test10()
    {
        Assert.IsFalse(Solve(1, 2, 3, 4, 5, 3, 5, 6));
    }
    
    [Test]
    public void Test11()
    {
        Assert.IsFalse(Solve(40, 50, 60, 10, 20, 30));
    }
    
    [Test]
    public void Test12()
    {
        Assert.IsTrue(Solve(1, 1));
    }
    
    [Test]
    public void Test13()
    {
        Assert.IsTrue(Solve(1, 2, 5, 3, 5));
    }
    
    [Test]
    public void Test14()
    {
        Assert.IsFalse(Solve(1, 2, 5, 5, 5));
    }
    
    [Test]
    public void Test15()
    {
        Assert.IsFalse(Solve(10, 1, 2, 3, 4, 5, 6, 1));
    }
    
    [Test]
    public void Test16()
    {
        Assert.IsTrue(Solve(1, 2, 3, 4, 3, 6));
    }
    
    [Test]
    public void Test17()
    {
        Assert.IsTrue(Solve(1, 2, 3, 4, 99, 5, 6));
    }
    
    [Test]
    public void Test18()
    {
        Assert.IsTrue(Solve(123, -17, -5, 1, 2, 3, 12, 43, 45));
    }
    
    [Test]
    public void Test19()
    {
        Assert.IsTrue(Solve(3, 5, 67, 98, 3));
    }

    private bool Solve(params int[] sequence) => Solution.solution(sequence);
}