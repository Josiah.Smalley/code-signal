-- setup
CREATE TABLE diary (
    id int,
    travel_date DATE,
    country varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT GROUP_CONCAT(DISTINCT country ORDER BY country SEPARATOR ';') FROM diary;
END;