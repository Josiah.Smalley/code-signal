using System.Linq;
using Common;

namespace Intro.RainbowOfClarity;

public class ChessKnight : IHaveSolution<string, int> {

    private static readonly (int h, int v)[] Moves = {
        (-2, 1),
        (-1, 2),
        (1, 2),
        (2, 1),
        (2, -1),
        (1, -2),
        (-1, -2),
        (-2, -1),
    };

    public int solution(string input) {
        var current = input.Select(c => (int)c).ToArray();

        return Moves.Count(move => isValid(current, move));
    }

    private bool isValid(int[] current, (int h, int v) move) {
        var col = current[0] + move.h;
        var row = current[1] + move.v;

        return (col is >= 'a' and <= 'h') && (row is >= '1' and <= '8');
    }

}
