using NUnit.Framework;
using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates;

public class MaxMultipleTests : BaseTest<MaxMultiple, MaxMultiple.MaxMultipleInput, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(MaxMultiple.MaxMultipleInput input) => Solution.solution(input);


}
