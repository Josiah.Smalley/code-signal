using System.Linq;

namespace Intro.IslandOfKnowledge;

public class Minesweeper {

    public int[][] solution(bool[][] matrix) {
        return Enumerable.Range(0, matrix.Length)
            .Select(r => Enumerable.Range(0, matrix[0].Length).Select(c => getCount(matrix, r, c)).ToArray())
            .ToArray();
    }

    private int getCount(bool[][] matrix, int row, int col) {
        var total = 0;

        for (var r = row - 1; r <= row + 1; r++) {
            for (var c = col - 1; c <= col + 1; c++) {
                if (r == row && c == col) {
                    continue;
                }

                total += isMine(matrix, r, c) ? 1 : 0;
            }
        }

        return total;
    }

    private bool isMine(bool[][] matrix, int row, int col) {
        if (row < 0 || row >= matrix.Length) {
            return false;
        }

        if (col < 0 || col >= matrix[0].Length) {
            return false;
        }

        return matrix[row][col];
    }

}
