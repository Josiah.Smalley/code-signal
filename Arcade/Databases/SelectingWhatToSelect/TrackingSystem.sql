-- setup
CREATE TABLE tracks
(
    received_at  DATETIME,
    event_name   VARCHAR(255),
    anonymous_id INT,
    user_id      INT NULL
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT DISTINCT anonymous_id AS anonym_id,
                    (SELECT T2.event_name
                     FROM tracks T2
                     WHERE T2.anonymous_id = tracks.anonymous_id
                       AND T2.user_id IS NULL
                     ORDER BY T2.received_at DESC
                     LIMIT 1)    AS last_null,
                    (SELECT T3.event_name
                     FROM tracks T3
                     WHERE T3.anonymous_id = tracks.anonymous_id
                       AND T3.user_id IS NOT NULL
                     ORDER BY T3.received_at
                     LIMIT 1)    AS first_notnull
    FROM tracks
    ORDER BY anonymous_id;
END;