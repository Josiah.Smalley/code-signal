using Intro.RainbowOfClarity;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainbowOfClarity;

public class LinesEncodingTests : BaseTest<LinesEncoding, string, string> {

    [TestCaseSource(typeof(DataSource))]
    public override string TestsFromFile(string input) => Solution.solution(input);


}
