-- setup
CREATE TABLE catalogs
(
    doc_id  int,
    xml_doc varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT REGEXP_REPLACE(xml_doc, '.*?<author>([^<]+)</author>.*', '$1') as author
    FROM catalogs
    ORDER BY author;
END