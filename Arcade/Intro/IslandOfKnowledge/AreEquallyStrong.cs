using System.Linq;

namespace Intro.IslandOfKnowledge;

public class AreEquallyStrong {

public bool solution(int yourLeft, int yourRight, int friendsLeft, int friendsRight) {
    return (new int[] { yourLeft, yourRight }).OrderBy(x => x)
        .Zip((new int[] { friendsLeft, friendsRight }).OrderBy(x => x))
        .All(pair => pair.First == pair.Second);
}

}
