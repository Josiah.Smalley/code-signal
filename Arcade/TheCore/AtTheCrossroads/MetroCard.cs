using Common;

namespace TheCore.AtTheCrossroads;

public class MetroCard : IHaveSolution<int, int[]> {

    public int[] solution(int input) { return input == 31 ? new[] { 28, 30, 31, } : new[] { 31, }; }

}
