using Common;

namespace TheCore.IntroGates; 

public class PhoneCall: IHaveSolution<PhoneCall.PhoneCallInput, int> {

    public class PhoneCallInput {

        public int min1 { get; set; }
        public int min2_10 { get; set; }
        public int min11 { get; set; }
        public int s { get; set; }

    }

    public int solution(PhoneCallInput input) => solution(input.min1, input.min2_10, input.min11, input.s);

    public int solution(int min1, int min2_10, int min11, int s) {
        var length = 0;

        if (s < min1) {
            return length;
        }

        length++;
        s -= min1;

        var extraMinutes = Math.Min(s / min2_10, 9);

        if (extraMinutes == 0) {
            return length;
        }

        length += extraMinutes;
        s -= extraMinutes * min2_10;

        if (extraMinutes < 9) {
            return length;
        }

        return length + (s / min11);
    }

}
