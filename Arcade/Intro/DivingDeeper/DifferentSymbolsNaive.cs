using System.Linq;
using Common;

namespace Intro.DivingDeeper; 

public class DifferentSymbolsNaive: IHaveSolution<string, int> {

    public int solution(string input) => input.ToHashSet().Count;

}
