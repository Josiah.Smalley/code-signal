-- setup
CREATE TABLE positions
(
    id INT,
    x  INT,
    y  INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Distances AS (SELECT P1.id AS id1, P2.id AS id2, POW(P1.x - P2.x, 2) + POW(P1.y - P2.y, 2) AS dist
                       FROM positions P1,
                            positions P2
                       WHERE P1.id != P2.id)
    SELECT id1, id2
    FROM positions
             INNER JOIN Distances ON id = id1
    WHERE dist = (SELECT MIN(dist) FROM Distances WHERE id1 = id)
    ORDER BY id1;

END;