using Intro.ThroughTheFog;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ThroughTheFog; 

public class CircleOfNumbersTests: BaseTest<CircleOfNumbers, CircleOfNumbers.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(CircleOfNumbers.Input input) => Solution.solution(input);

}
