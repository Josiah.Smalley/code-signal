-- setup
CREATE TABLE book_chapters
(
    chapter_name   VARCHAR(255),
    chapter_number VARCHAR(255)
);

-- solution
CREATE FUNCTION convert_roman(roman VARCHAR(255)) RETURNS INT
BEGIN

    SET @total := 0;

    -- handle I
    IF (LOCATE('IV', roman)) THEN
        SET @total := @total + 4;
        SET roman := REPLACE(roman, 'IV', '');
    ELSE
        IF (LOCATE('IX', roman)) THEN
            SET @total := @total + 9;
            SET roman := REPLACE(roman, 'IX', '');
        ELSE
            SET @total := @total + (SELECT LENGTH(roman) - LENGTH(REPLACE(roman, 'I', '')));
        END IF;
    END IF;

    -- handle V
    IF (LOCATE('V', roman)) THEN
        SET @total := @total + 5;
    END IF;

    -- handle X
    IF (LOCATE('XL', roman)) THEN
        SET @total := @total + 40;
        SET roman := REPLACE(roman, 'XL', '');
    ELSE
        IF (LOCATE('XC', roman)) THEN
            SET @total := @total + 90;
            SET roman := REPLACE(roman, 'XC', '');
        ELSE
            SET @total := @total + 10 * (SELECT LENGTH(roman) - LENGTH(REPLACE(roman, 'X', '')));
        END IF;
    END IF;

    -- handle L
    IF (LOCATE('L', roman)) THEN
        SET @total := @total + 50;
    END IF;

    -- handle C
    IF (LOCATE('CD', roman)) THEN
        SET @total := @total + 400;
        SET roman := REPLACE(roman, 'CD', '');
    ELSE
        IF (LOCATE('CM', roman)) THEN
            SET @total := @total + 900;
            SET roman := REPLACE(roman, 'CM', '');
        ELSE
            SET @total := @total + 100 * (SELECT LENGTH(roman) - LENGTH(REPLACE(roman, 'C', '')));
        END IF;
    END IF;

    -- handle D
    IF (LOCATE('D', roman)) THEN
        SET @total := @total + 500;
    END IF;

    RETURN @total;
END;

CREATE PROCEDURE solution()
BEGIN
    SELECT chapter_name -- , chapter_number, convert_roman(chapter_number) chapter
    FROM book_chapters
    ORDER BY convert_roman(chapter_number);
END;