using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge;

public class AvoidObstaclesTests : BaseTest<AvoidObstacles, int[], int> {


    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int[] input) => Solution.solution(input);

}
