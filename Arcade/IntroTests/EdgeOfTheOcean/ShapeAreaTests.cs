using Intro.EdgeOfTheOcean;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EdgeOfTheOcean;

public class ShapeAreaTests : BaseTest<ShapeArea>
{
    [Test]
    public void Test1()
    {
        Assert.AreEqual(5, Solve(2));
    }

    [Test]
    public void Test2()
    {
        Assert.AreEqual(13, Solve(3));
    }

    [Test]
    public void Test3()
    {
        Assert.AreEqual(1, Solve(1));
    }

    [Test]
    public void Test4()
    {
        Assert.AreEqual(41, Solve(5));
    }

    [Test]
    public void Test5()
    {
        Assert.AreEqual(97986001, Solve(7000));
    }

    [Test]
    public void Test6()
    {
        Assert.AreEqual(127984001, Solve(8000));
    }

    [Test]
    public void Test7()
    {
        Assert.AreEqual(199940005, Solve(9999));
    }

    [Test]
    public void Test8()
    {
        Assert.AreEqual(199900013, Solve(9998));
    }

    [Test]
    public void Test9()
    {
        Assert.AreEqual(161946005, Solve(8999));
    }

    [Test]
    public void Test10()
    {
        Assert.AreEqual(19801, Solve(100));
    }


    private int Solve(int n) => Solution.solution(n);
}