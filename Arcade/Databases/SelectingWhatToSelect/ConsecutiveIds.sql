-- setup
CREATE TABLE itemIds
(
    id INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT id, (@newId := @newId + 1) AS new_id
    FROM itemIds
             CROSS JOIN (SELECT @newId := 0) AS placeholder
    ORDER BY new_id;
END;