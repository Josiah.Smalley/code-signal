using Intro.ExploringTheWaters;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ExploringTheWaters; 

public class AlternatingSumsTests: BaseTest<AlternatingSums> {

    [Test]
    public void Test1() {
        var input = new int[]{50, 60, 60, 45, 70};
        var expected = new int[]{180, 105};
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test2() {
        var input = new int[]{100, 50};
        var expected = new int[]{100, 50};
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test3() {
        var input = new int[]{80};
        var expected = new int[]{80, 0};
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test4() {
        var input = new int[]{100, 50, 50, 100};
        var expected = new int[]{150, 150};
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test5() {
        var input = new int[]{100, 51, 50, 100};
        var expected = new int[]{150, 151};
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }

}
