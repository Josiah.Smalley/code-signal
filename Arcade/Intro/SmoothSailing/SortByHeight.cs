using System.Linq;

namespace Intro.SmoothSailing; 

public class SortByHeight {

    public int[] solution(int[] a) {
        using var sortedPeople = a.Where(v => v != -1).OrderBy(v => v).GetEnumerator();
        var i = 0;

        for (var j = 0; j < a.Length; j++) {
            if (a[j] == -1) {
                continue;
            }

            sortedPeople.MoveNext();
            a[j] = sortedPeople.Current;
        }

        return a;
    }

}
