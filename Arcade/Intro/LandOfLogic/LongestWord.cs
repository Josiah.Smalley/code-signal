using System.Linq;
using System.Text.RegularExpressions;
using Common;

namespace Intro.LandOfLogic;

public class LongestWord : IHaveSolution<string, string> {

    public string solution(string input) {
        return Regex.Matches(input, @"[a-zA-Z]+")
            .MaxBy(s => s.Value.Length)
            .Value;
    }

}
