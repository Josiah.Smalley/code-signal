-- setup
CREATE TABLE pr_department
(
    id          INT,
    name        VARCHAR(255),
    date_joined DATE
);
CREATE TABLE it_department
(
    id          INT,
    name        VARCHAR(255),
    date_joined DATE
);
CREATE TABLE sales_department
(
    id          INT,
    name        VARCHAR(255),
    date_joined DATE
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Latest AS ((SELECT name, 0 AS sort_order
                     FROM pr_department
                     ORDER BY date_joined DESC
                     LIMIT 5)
                    UNION ALL
                    (SELECT name, 1 AS sort_order
                     FROM it_department
                     ORDER BY date_joined DESC
                     LIMIT 5)
                    UNION ALL
                    (SELECT name, 2 AS sort_order
                     FROM SALES_department
                     ORDER BY date_joined DESC
                     LIMIT 5))
    SELECT name
    FROM Latest
    ORDER BY sort_order, name;

END;