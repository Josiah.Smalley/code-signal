-- setup
CREATE TABLE inspections
(
    inspection_id INT,
    driver_name   VARCHAR(255),
    date          DATE,
    miles_logged  INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    DROP TEMPORARY TABLE IF EXISTS results;
    CREATE TEMPORARY TABLE results
    (
        summary     VARCHAR(1000),
        driver_name VARCHAR(255) NULL DEFAULT NULL,
        date        DATE         NULL DEFAULT NULL
    );

    -- insert the first row
    INSERT INTO results (summary)
    VALUES (CONCAT(' Total miles driven by all drivers combined: ', (SELECT SUM(miles_logged)
                                                                     FROM inspections)));
    -- insert the summary for each driver
    INSERT INTO results (summary, driver_name)
    SELECT CONCAT(' Name: ', driver_name, '; number of inspections: ', COUNT(*), '; miles driven: ',
                  SUM(miles_logged)) AS summary,
           driver_name
    FROM inspections
    GROUP BY driver_name;

    -- insert all inspection details
    INSERT INTO results (summary, driver_name, date)
    SELECT CONCAT('  date: ', DATE_FORMAT(date, '%Y-%m-%d'), '; miles covered: ', miles_logged) AS summary,
           driver_name,
           date
    FROM inspections;

    -- sort and return
    SELECT summary FROM results
    ORDER BY driver_name, date;

END;