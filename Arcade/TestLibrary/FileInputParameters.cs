#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using NUnit.Framework;

namespace TestLibrary {
    public class FileInputParameters<TIn, TOut> : IEnumerable {

        protected virtual string? DataDirectory => null;
        protected virtual string FileName => "NA";

        private string GetFilePath() {
            var dataDirectory = DataDirectory ?? Environment.GetEnvironmentVariable("TestDataDirectory");

            if (dataDirectory == null) {
                throw new NullReferenceException("Cannot get data directory for test data");
            }

            return Path.Combine(dataDirectory, $"{FileName}.json");
        }

        public IEnumerator GetEnumerator() {
            var fileData =
                JsonConvert.DeserializeObject<IDictionary<string, BaseTestCase<TIn, TOut>>>(File.ReadAllText(GetFilePath()));
            
            foreach (var (key, value) in fileData) {
                // var data = new TestCaseData(value.Input)
                    // .SetName(key)
                    // .Returns(value.Expected);
                // data.SetName(key);
                // yield return data;
                // yield return new object[] { value };
                yield return new TestCaseData(value.Input).SetName($"Test: {key}").Returns(value.Expected);
            }
        }

    }

    public class FileInputParameters<TSolution, TIn, TOut> : FileInputParameters<TIn, TOut> {

        protected override string FileName => typeof(TSolution).Name;

    }
}
