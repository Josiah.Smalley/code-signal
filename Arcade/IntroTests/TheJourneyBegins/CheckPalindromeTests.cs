using Intro.TheJourneyBegins;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.TheJourneyBegins {
    public class CheckPalindromeTests: BaseTest<CheckPalindrome> {

        [Test]
        public void Test1() {
            Assert.IsTrue(Solution.solution("aabaa"));
        }
        
        [Test]
        public void Test2() {
            Assert.IsFalse(Solution.solution("abac"));
        }
        
        [Test]
        public void Test3() {
            Assert.IsTrue(Solution.solution("a"));
        }
        
        [Test]
        public void Test4() {
            Assert.IsFalse(Solution.solution("az"));
        }
        
        [Test]
        public void Test5() {
            Assert.IsTrue(Solution.solution("abacaba"));
        }
        
        [Test]
        public void Test6() {
            Assert.IsTrue(Solution.solution("z"));
        }
        
        [Test]
        public void Test7() {
            Assert.IsFalse(Solution.solution("aaabaaaa"));
        }

        [Test]
        public void Test8() {
            Assert.IsFalse(Solution.solution("zzzazzazz"));
        }
        
        [Test]
        public void Test9() {
            Assert.IsTrue(Solution.solution("hlbeeykoqqqqokyeeblh"));
        }
        
        [Test]
        public void Test10() {
            Assert.IsTrue(Solution.solution("hlbeeykoqqqokyeeblh"));
        }

    }
}
