-- setup
CREATE TABLE purchases
(
    timestamp  DATETIME,
    buyer_name VARCHAR(255)
);

-- solution 
CREATE PROCEDURE solution()
BEGIN

    WITH Indexed AS (SELECT buyer_name, (@idx := @idx + 1) AS idx
                     FROM purchases,
                          (SELECT @idx := 0) AS placeholder
                     ORDER BY timestamp)
    SELECT DISTINCT buyer_name
    FROM Indexed
    WHERE idx % 4 = 0
    ORDER BY buyer_name;
END;