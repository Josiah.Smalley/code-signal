using System.Text.RegularExpressions;
using Common;

namespace Intro.RainsOfReason; 

public class VariableName: IHaveSolution<string, bool> {

    public bool solution(string name) {
        const string pattern = @"^[a-zA-Z_][a-zA-Z0-9_]*$";
        return Regex.IsMatch(name, pattern);
    } 

}
