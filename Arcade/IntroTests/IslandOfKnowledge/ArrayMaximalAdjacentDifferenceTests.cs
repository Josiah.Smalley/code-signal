using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge;

public class ArrayMaximalAdjacentDifferenceTests : BaseTest<ArrayMaximalAdjacentDifference> {

    private class DataSource : FileInputParameters<int[], int> {
    
        protected override string FileName => "ArrayMaximalAdjacentDifference";
    
    }
    
    [TestCaseSource(typeof(DataSource))]
    public int TestsFromFile(int[] input) => Solution.solution(input);

}
