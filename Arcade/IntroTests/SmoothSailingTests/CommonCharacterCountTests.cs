using Intro.SmoothSailing;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.SmoothSailingTests; 

public class CommonCharacterCountTests: BaseTest<CommonCharacterCount> {

    [Test]
    public void Test1() {
        var string1 = "aabcc";
        var string2 = "adcaa";
        var expected = 3;
        var actual = Solution.solution(string1, string2);

        Assert.IsTrue(expected == actual);
    }
    
    [Test]
    public void Test2() {
        var string1 = "zzzz";
        var string2 = "zzzzzzz";
        var expected = 4;
        var actual = Solution.solution(string1, string2);

        Assert.IsTrue(expected == actual);
    }
    
    [Test]
    public void Test3() {
        var string1 = "abca";
        var string2 = "xyzbac";
        var expected = 3;
        var actual = Solution.solution(string1, string2);

        Assert.IsTrue(expected == actual);
    }
    
    [Test]
    public void Test4() {
        var string1 = "a";
        var string2 = "b";
        var expected = 0;
        var actual = Solution.solution(string1, string2);

        Assert.IsTrue(expected == actual);
    }
    
    [Test]
    public void Test5() {
        var string1 = "a";
        var string2 = "aaa";
        var expected = 1;
        var actual = Solution.solution(string1, string2);

        Assert.IsTrue(expected == actual);
    }

}
