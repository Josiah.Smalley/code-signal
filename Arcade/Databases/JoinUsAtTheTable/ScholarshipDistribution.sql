-- setup
CREATE TABLE candidates
(
    candidate_id   INT,
    candidate_name VARCHAR(255)
);

CREATE TABLE detentions
(
    detention_date DATE,
    student_id     INT
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT candidate_id AS student_id
    FROM candidates
             LEFT JOIN detentions ON candidate_id = detentions.student_id
    WHERE detention_date IS NULL
    ORDER BY candidate_id;
END