-- setup
CREATE TABLE flights
(
    flight_id INT,
    plane_id  INT
);

CREATE TABLE planes
(
    plane_id        INT,
    number_of_seats INT
);

CREATE TABLE purchases
(
    flight_id INT,
    seat_no   INT
);


-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT F.flight_id,
           P.number_of_seats - (SELECT COUNT(*)
                                FROM purchases Pur
                                WHERE Pur.flight_id = F.flight_id) AS free_seats
    FROM flights F
             INNER JOIN planes P ON F.plane_id = P.plane_id
    ORDER BY F.flight_id;
END;