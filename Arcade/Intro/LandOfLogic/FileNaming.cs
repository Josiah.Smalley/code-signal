using System.Collections.Generic;
using System.Linq;
using Common;

namespace Intro.LandOfLogic; 

public class FileNaming: IHaveSolution<string[], string[]> {

    public string[] solution(string[] names) {
        var nextIndex = new Dictionary<string, int>();

        return names.Select(name => {
                    if (!nextIndex.ContainsKey(name)) {
                        nextIndex[name] = 1;
                        return name;
                    }

                    var index = nextIndex[name];
                    var newName = $"{name}({index})";

                    while (nextIndex.ContainsKey(newName)) {
                        index += 1;
                        newName = $"{name}({index})";
                    }

                    nextIndex[newName] = 1;
                    nextIndex[name] = index + 1;

                    return newName;

                }
            )
            .ToArray();
    }

}
