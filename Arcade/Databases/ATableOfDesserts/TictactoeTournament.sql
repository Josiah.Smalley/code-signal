-- setup
CREATE TABLE results
(
    timestamp    DATETIME,
    name_naughts VARCHAR(255),
    name_crosses VARCHAR(255),
    board        VARCHAR(255)
);

-- solution
CREATE FUNCTION is_winner(player VARCHAR(1), board VARCHAR(255)) RETURNS INT
BEGIN
    SET @board := REPLACE(board, player, 'P');

    RETURN IF(@board LIKE 'PPP______' OR
              @board LIKE '___PPP___' OR
              @board LIKE '______PPP' OR
              @board LIKE 'P__P__P__' OR
              @board LIKE '_P__P__P_' OR
              @board LIKE '__P__P__P' OR
              @board LIKE 'P___P___P' OR
              @board LIKE '__P_P_P__',
              1,
              0);
END;

CREATE PROCEDURE solution()
BEGIN
    WITH PartialStats AS (SELECT name,
                                 IF((name = name_crosses AND is_winner('X', board))
                                        OR (name = name_naughts AND is_winner('O', board)), 1, 0) AS won,
                                 IF(is_winner('X', board) = is_winner('O', board), 1, 0)          AS draw
                          FROM (SELECT DISTINCT name
                                FROM (SELECT name_naughts AS name
                                      FROM results
                                      UNION
                                      SELECT name_crosses
                                      FROM results) N) Names
                                   INNER JOIN results R ON name = name_naughts OR name = name_crosses)
    SELECT name,
           SUM(won * 2 + draw) AS points,
           COUNT(name)         AS played,
           SUM(won)          AS won,
           SUM(draw)         AS draw,
           SUM(1 - won - draw) AS lost
    FROM PartialStats
    GROUP BY name
    ORDER BY points DESC, played, won DESC, name;

END;