using System.Linq;
using Common;

namespace Intro.EruptionOfLight;

public class ElectionWinners : IHaveSolution<ElectionWinners.Input, int> {

    public class Input {

        public int[] Votes { get; set; }
        public int K { get; set; }

    }

    public int solution(Input input) => solution(input.Votes, input.K);

    public int solution(int[] votes, int k) {
        var max = votes.Max();

        if (k != 0) {
            return votes.Count(v => v + k > max);
        }

        var numWithMax = votes.Count(v => v == max);
        return numWithMax == 1 ? 1 : 0;
    }


}
