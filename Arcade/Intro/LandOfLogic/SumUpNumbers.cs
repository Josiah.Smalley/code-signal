using System.Linq;
using System.Text.RegularExpressions;
using Common;

namespace Intro.LandOfLogic; 

public class SumUpNumbers : IHaveSolution<string, int> {

    public int solution(string input) => Regex.Split(input, @"[^0-9]+")
        .Select(str => int.TryParse(str, out var count) ? count : 0)
        .Sum();

}
