using Common;

namespace Intro.ThroughTheFog; 

public class StringRearrangement: IHaveSolution<string[], bool> {

    public bool solution(string[] inputArray) => hasSolution(inputArray, 0, inputArray.Length - 1);

    private static bool hasSolution(string[] words, int start, int end) {
        if (start == end) {
            return isValid(words);
        }

        for (var i = start; i <= end; i++) {
            swap(words, start, i);

            if (hasSolution(words, start + 1, end)) {
                return true;
            }

            swap(words, start, i);
        }

        return false;
    }

    private static void swap(string[] words, int a, int b) {
        (words[a], words[b]) = (words[b], words[a]);
    }

    private static bool isValid(string[] words) {
        for (var i = 0; i < words.Length - 1; i++) {
            if (!areConnected(words[i], words[i + 1])) {
                return false;
            }
        }

        return true;
    }

    private static bool areConnected(string first, string second) {
        if (first == second) {
            return false;
        }
        
        var differenceFound = false;

        for (var i = 0; i < first.Length; i++) {
            
            if (first[i] == second[i]) {
                continue;
            }

            if (differenceFound) {
                return false; // two differences found
            }

            differenceFound = true;
        }

        return true; // not the same and two differences not found => one difference found
    }

}
