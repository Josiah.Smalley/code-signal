using System;
using System.Collections.Generic;
using System.Linq;
using Common;

namespace Intro.DivingDeeper; 

public class ArrayMaxConsecutiveSum: IHaveSolution<ArrayMaxConsecutiveSum.Input, int> {

    public class Input {

        public int[] InputArray { get; set; }
        public int K { get; set; }

    }

    public int solution(Input input) => solution(input.InputArray, input.K);

    public int solution(int[] inputArray, int k) {
        var result = 0;
        var current = 0;
        var queue = new Queue<int>();
        
        foreach (var i in inputArray)
        {
            if (i < 0) {
                result = Math.Max(result, current);
                queue.Clear();
                continue;
            }

            if (queue.Count == k) {
                result = Math.Max(result, current);
                current -= queue.Dequeue();
            }

            queue.Enqueue(i);
            current += i;
        }

        result = Math.Max(result, current);

        return result;
    }

}
