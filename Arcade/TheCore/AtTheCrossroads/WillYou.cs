using Common;

namespace TheCore.AtTheCrossroads;

public class WillYou : IHaveSolution<WillYou.WillYouInput, bool> {

    public class WillYouInput {

        public bool young { get; set; }
        public bool beautiful { get; set; }
        public bool loved { get; set; }

    }

    public bool solution(WillYouInput input) => solution(input.young, input.beautiful, input.loved);

    public bool solution(bool young, bool beautiful, bool loved) =>
        (young && beautiful && !loved) ||
        (loved && (!young || !beautiful));


}
