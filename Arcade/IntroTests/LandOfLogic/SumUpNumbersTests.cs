using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic; 

public class SumUpNumbersTests : BaseTest<SumUpNumbers, string, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(string input) => Solution.solution(input);

    

}
