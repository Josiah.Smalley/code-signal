-- setup
CREATE TABLE locations_of_ships
(
    id             INT,
    upper_left_x   INT,
    upper_left_y   INT,
    bottom_right_x INT,
    bottom_right_y INT
);

CREATE TABLE opponents_shots
(
    id       INT,
    target_x INT,
    target_y INT
);

CREATE TABLE segments
(
    x       INT,
    y       INT,
    ship_id INT
);

-- solution
CREATE PROCEDURE add_ship(ship_id INT, x1 INT, y1 INT, x2 INT, y2 INT)
BEGIN
    SET @x := x1;
    SET @y := y1;

    INSERT INTO segments(x, y, ship_id) VALUES (@x, @y, ship_id);

    WHILE (@x < x2 OR @y < y2)
        DO
            IF (@x < x2) THEN
                SET @x := @x + 1;
            ELSE
                SET @y := @y + 1;
            END IF;

            INSERT INTO segments(x, y, ship_id) VALUES (@x, @y, ship_id);
        END WHILE;
END;

CREATE PROCEDURE solution()
BEGIN
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE _shipId BIGINT UNSIGNED;
    DECLARE x1 INT;
    DECLARE x2 INT;
    DECLARE y1 INT;
    DECLARE y2 INT;
    DECLARE cur CURSOR FOR SELECT id FROM locations_of_ships;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done := TRUE;

    DROP TEMPORARY TABLE IF EXISTS segments;
    CREATE TEMPORARY TABLE segments
    (
        x       INT,
        y       INT,
        ship_id INT
    );

    -- POPULATE THE SEGMENTS
    OPEN cur;

    populateLoop:
    LOOP
        FETCH cur INTO _shipId;
        IF done THEN
            LEAVE populateLoop;
        END IF;

        SELECT upper_left_x,
               upper_left_y,
               bottom_right_x,
               bottom_right_y
        INTO x1, y1, x2, y2
        FROM locations_of_ships
        WHERE id = _shipId;

        CALL add_ship(_shipId, x1, y1, x2, y2);

    END LOOP;

    CLOSE cur;

    -- FIRE MISSILES
    DELETE
    FROM segments S
    WHERE EXISTS(SELECT * FROM opponents_shots OP WHERE OP.target_x = S.x AND OP.target_y = S.y);

    -- DETERMINE STATUS
    WITH ShipStatus AS (SELECT Ships.id                                        AS ship_id,
                               (Ships.bottom_right_y - Ships.upper_left_y + 1) *
                               (Ships.bottom_right_x - Ships.upper_left_x + 1) AS size,
                               COUNT(segments.x)                               AS unhit
                        FROM locations_of_ships Ships
                                 LEFT JOIN segments ON Ships.id = segments.ship_id
                        GROUP BY Ships.id, size)
    SELECT size,
           SUM(IF(size = unhit, 1, 0))               AS undamaged,
           SUM(IF(unhit > 0 AND unhit < size, 1, 0)) AS partly_damaged,
           SUM(IF(0 = unhit, 1, 0))                  AS sunk
    FROM ShipStatus
    GROUP BY size
    ORDER BY size;

END;