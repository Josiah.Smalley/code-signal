using NUnit.Framework;
using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates;

public class CircleOfNumbersTests : BaseTest<CircleOfNumbers, CircleOfNumbers.CircleOfNumbersInput, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(CircleOfNumbers.CircleOfNumbersInput input) => Solution.solution(input);


}
