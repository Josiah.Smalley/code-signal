CREATE TABLE mischief
(
    mischief_date DATE,
    author        VARCHAR(255),
    title         VARCHAR(255)
);

CREATE PROCEDURE solution()
BEGIN
    SELECT WEEKDAY(mischief_date) weekday, mischief.*
    FROM mischief
    ORDER BY weekday, CASE WHEN author = 'Huey' THEN 0 WHEN author = 'Dewey' THEN 1 ELSE 2 END, mischief_date, title;
END;