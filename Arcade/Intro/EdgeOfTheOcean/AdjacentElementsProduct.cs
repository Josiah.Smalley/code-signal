using System.Linq;

namespace Intro.EdgeOfTheOcean
{
    public class AdjacentElementsProduct
    {
        public int solution(int[] inputArray)
        {
            return inputArray.Select(
                (value, index) => index == 0 ? int.MinValue : value * inputArray[index - 1]
            ).Max();
        }
    }
}