-- setup
CREATE TABLE movies
(
    movie VARCHAR(255),
    genre VARCHAR(255)
);

CREATE TABLE starring_actors
(
    id         INT,
    movie_name VARCHAR(255),
    actor      VARCHAR(255)
);

CREATE TABLE actor_ages
(
    actor VARCHAR(255),
    age   INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SET @genre := (SELECT genre
                   FROM movies
                   GROUP BY genre
                   ORDER BY COUNT(*) DESC
                   LIMIT 1);

    SELECT DISTINCT AA.actor, age
    FROM actor_ages AA
             INNER JOIN starring_actors SA ON AA.actor = SA.actor
             INNER JOIN movies M ON SA.movie_name = M.movie
    WHERE M.genre = @genre
    ORDER BY age DESC, AA.actor;
END;