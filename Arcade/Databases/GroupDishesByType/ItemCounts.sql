-- setup
CREATE TABLE availableItems
(
    id        int,
    item_name varchar(255),
    item_type varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT item_name, item_type, COUNT(*) as item_count
    FROM availableItems
    GROUP BY item_name, item_type
    ORDER BY item_type, item_name;

END;