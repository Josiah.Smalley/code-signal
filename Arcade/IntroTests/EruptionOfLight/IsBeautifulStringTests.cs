using Intro.EruptionOfLight;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EruptionOfLight;

public class IsBeautifulStringTests : BaseTest<IsBeautifulString, string, bool> {


    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(string input) => Solution.solution(input);

}
