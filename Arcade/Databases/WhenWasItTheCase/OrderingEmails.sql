-- setup
CREATE TABLE emails
(
    id          INT,
    email_title VARCHAR(255),
    size        INT
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT id,
           email_title,
           IF(size < 1048576, CONCAT(FLOOR(size / 1024), ' Kb'), CONCAT(FLOOR(size / 1048576), ' Mb')) AS short_size
    FROM emails
    ORDER BY size DESC;
END