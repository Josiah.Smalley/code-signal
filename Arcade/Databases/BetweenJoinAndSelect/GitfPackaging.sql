-- setup
CREATE TABLE gifts
(
    id        INT,
    gift_name VARCHAR(255),
    length    INT,
    width     INT,
    height    INT
);

CREATE TABLE packages
(
    package_type VARCHAR(255),
    length       INT,
    width        INT,
    height       INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Possibilities AS (SELECT id, gift_name, package_type, p.width * p.length * p.height AS vol
                           FROM gifts g
                                    INNER JOIN packages p ON
                               g.length <= p.length AND g.width <= p.width AND g.height <= p.height),
         Assignments AS (SELECT package_type
                         FROM Possibilities
                         WHERE NOT EXISTS(SELECT *
                                          FROM Possibilities P2
                                          WHERE Possibilities.id = P2.id
                                            AND P2.vol < Possibilities.vol))
    SELECT package_type, COUNT(*) AS number
    FROM Assignments
    GROUP BY package_type
    ORDER BY package_type;
END;