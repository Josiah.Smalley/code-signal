using Common;

namespace TheCore.IntroGates;

public class AddTwoDigits : IHaveSolution<int, int> {

    public int solution(int input) => (input % 10) + (input / 10);

}
