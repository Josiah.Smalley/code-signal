using System.Collections.Generic;
using System.Linq;

namespace Intro.IslandOfKnowledge; 

public class AvoidObstacles {

    public int solution(int[] inputArray) {
        var badSteps = new HashSet<int>(inputArray);
        var stepSize = 2;
        while (badSteps.Any(step => step%stepSize == 0)) {
            stepSize++;
        }

        return stepSize;
    }

}
