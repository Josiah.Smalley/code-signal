-- setup
CREATE TABLE people_interests(
    name varchar(255),
    interests set('reading','sports','swimming','drawing','writing','acting','cooking','dancing','fishkeeping','juggling','sculpting','videogaming')
);

-- solution
CREATE PROCEDURE solution()
SELECT name
FROM people_interests
WHERE interests & (FIND_IN_SET('reading', interests) > 0) AND interests & (FIND_IN_SET('drawing', interests) > 0)
ORDER BY name
