-- setup
CREATE TABLE documents
(
    id   INT,
    date_str VARCHAR(10)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT DATE(date_str) AS date_iso
    FROM documents
    ORDER BY id;
END