using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates; 

public class LargestNumberTests : BaseTest<LargestNumber, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);

    

}
