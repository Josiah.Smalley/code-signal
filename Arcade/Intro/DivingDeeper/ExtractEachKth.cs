using System.Linq;
using Common;

namespace Intro.DivingDeeper;

public class ExtractEachKth : IHaveSolution<ExtractEachKth.Input, int[]> {

    public class Input {

        public int[] InputArray { get; set; }
        public int K { get; set; }

    }

    public int[] solution(Input input) => solution(input.InputArray, input.K).ToArray();

    public int[] solution(int[] inputArray, int k) => inputArray.Where((_, idx) => ((idx + 1) % k) != 0).ToArray();

}
