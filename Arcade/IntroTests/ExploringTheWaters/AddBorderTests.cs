using Intro.ExploringTheWaters;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ExploringTheWaters;

public class AddBorderTests : BaseTest<AddBorder> {


    [Test]
    public void Test1() {
        var inputArray = new[] { "abc", "ded" };
        var expected = new[] { "*****", "*abc*", "*ded*", "*****" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test2() {
        var inputArray = new[] { "a" };
        var expected = new[] { "***", "*a*", "***" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test3() {
        var inputArray = new[] { "aa", "**", "zz" };
        var expected = new[] { "****", "*aa*", "****", "*zz*", "****" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test4() {
        var inputArray = new[] { "abcde", "fghij", "klmno", "pqrst", "uvwxy" };
        var expected = new[] { "*******", "*abcde*", "*fghij*", "*klmno*", "*pqrst*", "*uvwxy*", "*******" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test5() {
        var inputArray = new[] { "wzy**" };
        var expected = new[] { "*******", "*wzy***", "*******" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

}
