using System.Linq;

namespace Intro.RainsOfReason; 

public class ArrayReplace {

    public int[] solution(int[] inputArray, int elemToReplace, int substitutionElem) {
        return inputArray.Select(x => x == elemToReplace ? substitutionElem : x).ToArray();
    }

}
