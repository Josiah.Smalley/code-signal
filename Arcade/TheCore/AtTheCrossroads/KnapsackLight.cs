using Common;

namespace TheCore.AtTheCrossroads;

public class KnapsackLight : IHaveSolution<KnapsackLight.KnapsackLightInput, int> {

    public class KnapsackLightInput {

        public int value1 { get; set; }
        public int weight1 { get; set; }
        public int value2 { get; set; }
        public int weight2 { get; set; }
        public int maxW { get; set; }

    }

    public int solution(KnapsackLightInput input) =>
        solution(input.value1,
            input.weight1,
            input.value2,
            input.weight2,
            input.maxW
        );

    public int solution(
        int value1,
        int weight1,
        int value2,
        int weight2,
        int maxW
    ) {
        var options = new (int weight, int value)[] {
            (0, 0), (weight1, value1), (weight2, value2), (weight1 + weight2, value1 + value2)
        };

        return options.OrderBy(x => x.value).Last(x => x.weight <= maxW).value;
    }


}
