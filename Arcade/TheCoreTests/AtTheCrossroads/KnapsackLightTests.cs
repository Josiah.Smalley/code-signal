using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class KnapsackLightTests : BaseTest<KnapsackLight, KnapsackLight.KnapsackLightInput, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(KnapsackLight.KnapsackLightInput input) => Solution.solution(input);

    

}
