using Common;

namespace Intro.ThroughTheFog; 

public class AbsoluteValuesSumMinimization: IHaveSolution<int[], int> {

    public int solution(int[] a) => a[(a.Length - 1) >> 1];

}
