using Common;

namespace TheCore.AtTheCrossroads; 

public class ArithmeticExpression: IHaveSolution<ArithmeticExpression.ArithmeticExpressionInput, bool> {

    public class ArithmeticExpressionInput {

        public int a { get; set; }
        public int b { get; set; }
        public int c { get; set; }

    }

    public bool solution(ArithmeticExpressionInput input) => solution(input.a, input.b, input.c);

    public bool solution(int a, int b, int c) =>
        a + b == c ||
        a - b == c ||
        a * b == c ||
        a == b * c;


}
