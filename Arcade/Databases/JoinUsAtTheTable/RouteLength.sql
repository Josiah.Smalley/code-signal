-- setup
CREATE TABLE cities
(
    id INT,
    x  INT,
    y  INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Segments AS (SELECT SQRT(POW(C1.x - C2.x, 2) + POW(C1.y - C2.y, 2)) AS length
                      FROM cities C1
                               INNER JOIN cities C2 ON C1.id + 1 = C2.id)
    SELECT ROUND(SUM(length), 9)
    FROM Segments;
END