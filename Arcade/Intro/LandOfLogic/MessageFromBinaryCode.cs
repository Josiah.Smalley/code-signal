using System;
using System.Linq;
using Common;

namespace Intro.LandOfLogic;

public class MessageFromBinaryCode : IHaveSolution<string, string> {


    public string solution(string code) =>
        string.Join("",
            Enumerable.Range(0, code.Length >> 3)
                .Select(start => (char)Convert.ToInt32(code.Substring(start*8, 8), 2))
        );

}
