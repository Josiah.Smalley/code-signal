using System;
using Common;

namespace Intro.DarkWilderness; 

public class GrowingPlant: IHaveSolution<GrowingPlant.Input, int> {

    public class Input {

        public int UpSpeed { get; set; }
        public int DownSpeed { get; set; }
        public int DesiredHeight { get; set; }

    }

    public int solution(Input input) => solution(input.UpSpeed, input.DownSpeed, input.DesiredHeight);

    public int solution(int upSpeed, int downSpeed, int desiredHeight) =>
        Math.Max(1, (int)Math.Ceiling((desiredHeight - downSpeed) / (float)(upSpeed - downSpeed)));

}
