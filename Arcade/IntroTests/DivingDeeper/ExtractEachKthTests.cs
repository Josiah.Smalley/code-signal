using Intro.DivingDeeper;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DivingDeeper; 

public class ExtractEachKthTests: BaseTest<ExtractEachKth, ExtractEachKth.Input, int[]> {

    [TestCaseSource(typeof(DataSource))]
    public override int[] TestsFromFile(ExtractEachKth.Input input) => Solution.solution(input);

}
