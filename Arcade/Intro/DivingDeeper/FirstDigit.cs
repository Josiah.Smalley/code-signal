using System.Linq;
using Common;

namespace Intro.DivingDeeper; 

public class FirstDigit: IHaveSolution<string, char> {

    public char solution(string input) => input.First(c => c is >= '0' and <= '9');

}
