using Intro.DarkWilderness;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DarkWilderness;

public class GrowingPlantTests : BaseTest<GrowingPlant, GrowingPlant.Input, int> {


    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(GrowingPlant.Input input) => Solution.solution(input);

}
