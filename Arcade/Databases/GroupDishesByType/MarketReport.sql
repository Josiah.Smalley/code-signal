-- setup
CREATE TABLE foreignCompetitors
(
    competitor varchar(255),
    country    varchar(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT IF(GROUPING(country), 'Total:', country) as country, COUNT(*) competitors
    FROM foreignCompetitors
    GROUP BY country
    WITH ROLLUP
    ORDER BY GROUPING(country), country;
END