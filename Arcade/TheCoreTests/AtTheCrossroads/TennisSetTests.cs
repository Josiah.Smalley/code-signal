using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class TennisSetTests : BaseTest<TennisSet, TennisSet.TennisSetInput, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(TennisSet.TennisSetInput input) => Solution.solution(input);

    

}
