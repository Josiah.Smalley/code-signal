using Common;

namespace Intro.RainsOfReason;

public class ChessBoardCellColor : IHaveSolution<ChessBoardCellColor.Input, bool> {

    public class Input {

        public string cell1 { get; set; }
        public string cell2 { get; set; }

    }

    public bool solution(Input input) => solution(input.cell1, input.cell2);

    public bool solution(string cell1, string cell2) => getParity(cell1) == getParity(cell2);

    private static bool getParity(string cell) => cell[0] % 2 == cell[1] % 2;

}
