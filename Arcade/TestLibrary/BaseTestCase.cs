namespace TestLibrary {
    public class BaseTestCase<TIn, TOut> {

        public TIn Input { get; set; }
        public TOut Expected { get; set; }

    }
}
