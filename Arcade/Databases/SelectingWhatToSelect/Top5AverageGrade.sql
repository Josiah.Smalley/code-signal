-- setup
CREATE TABLE students
(
    student_id   INT,
    student_name VARCHAR(255),
    GRADE        FLOAT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT ROUND(AVG(grade), 2) average_grade
    FROM (SELECT *
          FROM students
          ORDER BY grade DESC
          LIMIT 5) Top5;
END