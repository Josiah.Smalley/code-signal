-- setup
CREATE TABLE scores
(
    match_id          INT,
    first_team_score  INT,
    second_team_score INT,
    match_host        INT
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Stats AS (SELECT match_id,
                          first_team_score,
                          second_team_score,
                          match_host,
                          IF(first_team_score > scores.second_team_score, 1, 0) AS first_won,
                          IF(first_team_score < scores.second_team_score, 1, 0) AS second_won,
                          first_team_score - second_team_score                  AS score_difference,
                          IF(match_host = 1, 0, first_team_score)               AS first_scores_as_away,
                          IF(match_host = 2, 0, second_team_score)              AS second_scores_as_away
                   FROM scores)
    SELECT CASE
               WHEN SUM(first_won) > SUM(second_won) THEN 1
               WHEN SUM(second_won) > SUM(first_won) THEN 2
               WHEN SUM(score_difference) > 0 THEN 1
               WHEN SUM(score_difference) < 0 THEN 2
               WHEN SUM(first_scores_as_away) > SUM(second_scores_as_away) THEN 1
               WHEN SUM(second_scores_as_away) > SUM(first_scores_as_away) THEN 2
               ELSE 0
               END AS winner
    FROM Stats;
END