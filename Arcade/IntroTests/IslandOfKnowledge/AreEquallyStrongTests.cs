using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge;

public class AreEquallyStrongTests : BaseTest<AreEquallyStrong> {

    [Test]
    public void Test1() {
        var yourLeft = 10;
        var yourRight = 15;
        var friendsLeft = 15;
        var friendsRight = 10;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test2() {
        var yourLeft = 15;
        var yourRight = 10;
        var friendsLeft = 15;
        var friendsRight = 10;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test3() {
        var yourLeft = 15;
        var yourRight = 10;
        var friendsLeft = 15;
        var friendsRight = 9;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test4() {
        var yourLeft = 10;
        var yourRight = 5;
        var friendsLeft = 5;
        var friendsRight = 10;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test5() {
        var yourLeft = 10;
        var yourRight = 15;
        var friendsLeft = 5;
        var friendsRight = 20;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test6() {
        var yourLeft = 10;
        var yourRight = 20;
        var friendsLeft = 10;
        var friendsRight = 20;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test7() {
        var yourLeft = 5;
        var yourRight = 20;
        var friendsLeft = 20;
        var friendsRight = 5;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test8() {
        var yourLeft = 20;
        var yourRight = 15;
        var friendsLeft = 5;
        var friendsRight = 20;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test9() {
        var yourLeft = 5;
        var yourRight = 10;
        var friendsLeft = 5;
        var friendsRight = 10;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test10() {
        var yourLeft = 1;
        var yourRight = 10;
        var friendsLeft = 10;
        var friendsRight = 0;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test11() {
        var yourLeft = 5;
        var yourRight = 5;
        var friendsLeft = 10;
        var friendsRight = 10;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test12() {
        var yourLeft = 10;
        var yourRight = 5;
        var friendsLeft = 10;
        var friendsRight = 6;
        Assert.IsFalse(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test13() {
        var yourLeft = 1;
        var yourRight = 1;
        var friendsLeft = 1;
        var friendsRight = 1;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }
    
    [Test]
    public void Test14() {
        var yourLeft = 0;
        var yourRight = 10;
        var friendsLeft = 10;
        var friendsRight = 0;
        Assert.IsTrue(Solution.solution(yourLeft, yourRight, friendsLeft, friendsRight));
    }

}
