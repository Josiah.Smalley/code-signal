-- setup
CREATE TABLE expenditure_plan
(
    monday_date     DATE,
    expenditure_sum INT
);

CREATE TABLE allowable_expenditure
(
    id          INT,
    left_bound  INT,
    right_bound INT,
    value       INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT id,
           IF(SUM(expenditure_sum) <= value, 0, SUM(expenditure_sum) - value) AS loss
    FROM allowable_expenditure
             LEFT JOIN expenditure_plan ON left_bound <= WEEKOFYEAR(monday_date) AND right_bound >= WEEKOFYEAR(monday_date)
    GROUP BY id
    ORDER BY id;
END;
