using Intro.RainbowOfClarity;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainbowOfClarity; 

public class DeleteDigitTests : BaseTest<DeleteDigit, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);

    

}
