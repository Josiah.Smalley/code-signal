using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge;

public class MinesweeperTests : BaseTest<Minesweeper, bool[][], int[][]> {

    [TestCaseSource(typeof(DataSource))]
    public override int[][] TestsFromFile(bool[][] input) => Solution.solution(input);

}
