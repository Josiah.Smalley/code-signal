using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates;

public class CandiesTests : BaseTest<Candies, Candies.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(Candies.Input input) => Solution.solution(input);


}
