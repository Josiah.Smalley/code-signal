using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class ArithmeticExpressionTests : BaseTest<ArithmeticExpression, ArithmeticExpression.ArithmeticExpressionInput, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(ArithmeticExpression.ArithmeticExpressionInput input) => Solution.solution(input);

    

}
