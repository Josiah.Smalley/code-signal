-- setup
CREATE TABLE accounting
(
    date   DATE,
    profit INT UNSIGNED,
    loss   INT UNSIGNED
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT YEAR(date)              AS year,
           QUARTER(date)           AS quarter,
           SUM(profit) - SUM(loss) AS net_profit
    FROM accounting
    GROUP BY year, quarter
    ORDER BY year, quarter;
END