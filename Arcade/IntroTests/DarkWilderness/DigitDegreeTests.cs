using Intro.DarkWilderness;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DarkWilderness; 

public class DigitDegreeTests: BaseTest<DigitDegree, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);

}
