using System;

namespace Intro.IslandOfKnowledge;

public class BoxBlur {

    public int[][] solution(int[][] image) {
        var rowCount = image.Length;
        var colCount = image[0].Length;
        var result = new int[rowCount - 2][];

        for (var row = 1; row < rowCount - 1; row++) {
            result[row - 1] = new int[colCount - 2];

            for (var col = 1; col < colCount - 1; col++) {
                double sum = 0;

                for (var r = row - 1; r < row + 2; r++) {
                    for (var c = col - 1; c < col + 2; c++) {
                        sum += image[r][c];
                    }
                }

                result[row - 1][col - 1] = (int)Math.Floor(sum / 9);
            }
        }

        return result;
    }

}
