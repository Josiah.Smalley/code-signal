-- setup 
CREATE TABLE employees
(
    id         INT,
    full_name  VARCHAR(255),
    department INT
);

CREATE TABLE departments
(
    id       INT,
    dep_name VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT dep_name
    FROM departments
    WHERE NOT EXISTS(SELECT * FROM employees WHERE department = departments.id)
    ORDER BY id;
END;