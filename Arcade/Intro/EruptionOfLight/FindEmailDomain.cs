using Common;

namespace Intro.EruptionOfLight; 

public class FindEmailDomain: IHaveSolution<string, string> {

    public string solution(string address) {
        var atSymbol = address.LastIndexOf('@');
        return address[(atSymbol + 1)..];
    }


}
