using Intro.DivingDeeper;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DivingDeeper; 

public class DifferentSymbolsNaiveTests: BaseTest<DifferentSymbolsNaive, string, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(string input) => Solution.solution(input);

}
