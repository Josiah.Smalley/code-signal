using System;

namespace Intro.IslandOfKnowledge; 

public class ArrayMaximalAdjacentDifference {

public int solution(int[] inputArray) {
    var max = int.MinValue;

    for (var i = 1; i < inputArray.Length; i++) {
        var diff = Math.Abs(inputArray[i] - inputArray[i - 1]);
        if (diff > max) {
            max = diff;
        }
    }

    return max;
}

}
