using Intro.SmoothSailing;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.SmoothSailingTests;

public class AllLongestStringsTests : BaseTest<AllLongestStrings> {

    [Test]
    public void Test1() {
        var inputArray = new[] {
            "aba",
            "aa",
            "ad",
            "vcd",
            "aba"
        };

        var expected = new[] { "aba", "vcd", "aba" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test2() {
        var inputArray = new[] { "aa" };

        var expected = new[] { "aa" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test3() {
        var inputArray = new[] { "abc", "eeee", "abcd", "dcd" };

        var expected = new[] { "eeee", "abcd" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test4() {
        var inputArray = new[] {
            "a",
            "abc",
            "cbd",
            "zzzzzz",
            "a",
            "abcdef",
            "asasa",
            "aaaaaa"
        };

        var expected = new[] { "zzzzzz", "abcdef", "aaaaaa" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test5() {
        var inputArray = new[] { "enyky", "benyky", "yely", "varennyky" };
        var expected = new[] { "varennyky" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test6() {
        var inputArray = new[] { "abacaba", "abacab", "abac", "xxxxxx" };
        var expected = new[] { "abacaba" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test7() {
        var inputArray = new[] {
            "young",
            "yooooooung",
            "hot",
            "or",
            "not",
            "come",
            "on",
            "fire",
            "water",
            "watermelon"
        };

        var expected = new[] { "yooooooung", "watermelon" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test8() {
        var inputArray = new[] { "onsfnib", "aokbcwthc", "jrfcw" };
        var expected = new[] { "aokbcwthc" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test9() {
        var inputArray = new[] { "lbgwyqkry" };
        var expected = new[] { "lbgwyqkry" };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

    [Test]
    public void Test10() {
        var inputArray = new[] { "i", };
        var expected = new[] { "i", };
        var actual = Solution.solution(inputArray);
        Assert.AreEqual(expected, actual);
    }

}
