using Common;

namespace TheCore.IntroGates;

public class CircleOfNumbers : IHaveSolution<CircleOfNumbers.CircleOfNumbersInput, int> {

    public class CircleOfNumbersInput {

        public int n { get; set; }
        public int firstNumber { get; set; }

    }

    public int solution(CircleOfNumbersInput input) => solution(input.n, input.firstNumber);

    public int solution(int n, int firstNumber) => (firstNumber + (n / 2)) % n;


}
