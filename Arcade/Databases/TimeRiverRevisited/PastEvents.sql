-- setup
CREATE TABLE Events
(
    id         INT,
    name       VARCHAR(255),
    event_date DATE
);
-- solution
CREATE PROCEDURE solution()
BEGIN

    WITH DateData AS (SELECT DATE_SUB(MAX(event_date), INTERVAL 7 DAY) AS min_date, MAX(event_date) AS max_date
                      FROM Events)
    SELECT name, event_date
    FROM Events,
         DateData
    WHERE event_date >= min_date
      AND event_date < max_date
    ORDER BY event_date DESC;
END