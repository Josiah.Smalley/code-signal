using System.Linq;
using Common;

namespace Intro.EruptionOfLight;

public class IsBeautifulString : IHaveSolution<string, bool> {

    public bool solution(string input) {
        var counts = input.GroupBy(c => c).ToDictionary(group => (int)group.Key, group => group.Count());

        for (var i = (int)'b'; i <= 'z'; i++) {
            if (!counts.ContainsKey(i)) {
                continue;
            }

            if (!counts.TryGetValue(i - 1, out var previous)) {
                return false;
            }

            if (previous < counts[i]) {
                return false;
            }
        }

        return true;
    }

}
