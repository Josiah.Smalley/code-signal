using System.Linq;

namespace Intro.ExploringTheWaters; 

public class PalindromeRearranging {

public bool solution(string inputString) {
    var counts = new int[26];
    foreach (var c in inputString) {
        counts[c - 'a']++;
    }

    var numOdd = counts.Count(c => c % 2 == 1);
    var isOddLength = inputString.Length % 2 == 1;
    
    return numOdd == 0 || (numOdd == 1 && isOddLength);
}

}
