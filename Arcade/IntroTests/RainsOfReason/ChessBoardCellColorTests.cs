using Intro.RainsOfReason;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainsOfReason; 

public class ChessBoardCellColorTests: BaseTest<ChessBoardCellColor, ChessBoardCellColor.Input, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(ChessBoardCellColor.Input input) => Solution.solution(input);

}
