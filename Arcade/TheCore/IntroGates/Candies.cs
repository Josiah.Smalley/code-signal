using Common;

namespace TheCore.IntroGates;

public class Candies : IHaveSolution<Candies.Input, int> {

    public class Input {

        public int n { get; set; }
        public int m { get; set; }

    }

    public int solution(Input input) => solution(input.n, input.m);

    public int solution(int n, int m) => n * (m / n);

}
