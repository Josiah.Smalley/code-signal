using System;

namespace Intro.SmoothSailing;

public class IsLucky {

    public bool solution(int n) {
        var numDigits = (int)Math.Log10(n) + 1;
        var halfDigits = numDigits / 2;

        var total = 0;

        for (var i = 0; i < halfDigits; i++) {
            var digit = n % 10;
            total += digit;
            n /= 10;
        }
        
        for (var i = 0; i < halfDigits; i++) {
            var digit = n % 10;
            total -= digit;
            n /= 10;
        }
        
        return total == 0;
    }

}
