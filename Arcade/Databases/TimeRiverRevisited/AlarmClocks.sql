-- setup
CREATE TABLE userInput
(
    input_date DATETIME
);
-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH RECURSIVE Alarms AS (SELECT input_date AS alarm
                              FROM userInput
                              UNION ALL
                              SELECT alarm + INTERVAL 1 WEEK
                              FROM Alarms
                              WHERE YEAR(alarm) = (SELECT YEAR(input_date) FROM userInput))
    SELECT *
    FROM Alarms
    WHERE YEAR(alarm) = (SELECT YEAR(input_date) FROM userInput)
    ORDER BY alarm;
END