using Common;

namespace Intro.LandOfLogic; 

public class DigitsProduct: IHaveSolution<int, int> {

    public int solution(int input) {
        switch (input) {
            case 0:
                return 10;

            case 1:
                return 1;
        }

        var result = 0;
        var multiplier = 1;

        for (var f = 9; f > 1; f--) {
            if (input == 1) {
                break;
            }
            
            if (input % f != 0) {
                continue;
            }

            while (input % f == 0) {
                input /= f;
                result += (multiplier * f);
                multiplier *= 10;
            }
        }

        if (input != 1) {
            return -1;
        }

        return result;
    }

}
