using Intro.EruptionOfLight;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EruptionOfLight;

public class ElectionWinnersTests : BaseTest<ElectionWinners, ElectionWinners.Input, int> {


    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(ElectionWinners.Input input) => Solution.solution(input);

}
