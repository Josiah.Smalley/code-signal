using Intro.ExploringTheWaters;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ExploringTheWaters; 

public class AreSimilarTests: BaseTest<AreSimilar> {

    [Test]
    public void Test1() {
        var a = new[] {1, 2, 3};
        var b = new[] {1, 2, 3};
        Assert.IsTrue(Solution.solution(a, b));
    }
    
    [Test]
    public void Test2() {
        var a = new[] {1, 2, 3};
        var b = new[] {2, 1, 3};
        Assert.IsTrue(Solution.solution(a, b));
    }
    
    [Test]
    public void Test3() {
        var a = new[] {1, 2, 2};
        var b = new[] {2, 1, 1};
        Assert.IsFalse(Solution.solution(a, b));
    }
    
    [Test]
    public void Test4() {
        var a = new[] {1, 2, 1, 2};
        var b = new[] {2, 2, 1, 1};
        Assert.IsTrue(Solution.solution(a, b));
    }
    
    [Test]
    public void Test5() {
        var a = new[] {1,2,1,2,2,1};
        var b = new[] {2,2,1,1,2,1};
        Assert.IsTrue(Solution.solution(a, b));
    }
    
    [Test]
    public void Test6() {
        var a = new[] {1,1,4};
        var b = new[] {1,2,3};
        Assert.IsFalse(Solution.solution(a, b));
    }
    
    [Test]
    public void Test7() {
        var a = new[] {1,2,3};
        var b = new[] {1,10,2};
        Assert.IsFalse(Solution.solution(a, b));
    }
    
    [Test]
    public void Test8() {
        var a = new[] {2,3,1};
        var b = new[] {1,3,2};
        Assert.IsTrue(Solution.solution(a, b));
    }
    
    [Test]
    public void Test9() {
        var a = new[] {2,3,9};
        var b = new[] {10,3,2};
        Assert.IsFalse(Solution.solution(a, b));
    }
    
    [Test]
    public void Test10() {
        var a = new[] {832, 998, 148, 570, 533, 561, 894, 147, 455, 279};
        var b = new[] {832, 570, 148, 998, 533, 561, 455, 147, 894, 279};
        Assert.IsFalse(Solution.solution(a, b));
    }


    [Test]
    public void CustomTest1() {
        var a = new[] { 1, 2, 3, 4 };
        var b = new[] { 1, 2, 3, 5 };
        Assert.IsFalse(Solution.solution(a, b));
    }

}
