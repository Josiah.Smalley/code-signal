using Intro.EdgeOfTheOcean;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EdgeOfTheOcean
{
    public class AdjacentElementsProductTests : BaseTest<AdjacentElementsProduct>
    {
        [Test]
        public void Test1()
        {
            Assert.AreEqual(
                21,
                solution(new[] { 3, 6, -2, -5, 7, 3 })
            );
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(
                2,
                solution(new[] { -1, -2 })
            );
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual(
                6,
                solution(new[] { 5, 1, 2, 3, 1, 4 }));
        }

        [Test]
        public void Test4()
        {
            Assert.AreEqual(
                6,
                solution(new[] { 1, 2, 3, 0 })
            );
        }

        [Test]
        public void Test5()
        {
            Assert.AreEqual(
                50,
                solution(new[] { 9, 5, 10, 2, 24, -1, -48 })
            );
        }

        [Test]
        public void Test6()
        {
            Assert.AreEqual(
                30,
                solution(new[] { 5, 6, -4, 2, 3, 2, -23 })
            );
        }

        [Test]
        public void Test7()
        {
            Assert.AreEqual(
                6,
                solution(new[] { 4, 1, 2, 3, 1, 5 })
            );
        }

        [Test]
        public void Test8()
        {
            Assert.AreEqual(
                -12,
                solution(new[] { -23, 4, -3, 8, -12 })
            );
        }

        [Test]
        public void Test9()
        {
            Assert.AreEqual(
                0,
                solution(new[] { 1, 0, 1, 0, 1000 })
            );
        }

        private int solution(int[] input) => Solution.solution(input);
    }
}