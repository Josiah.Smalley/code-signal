-- setup
CREATE TABLE users
(
    id    int,
    login varchar(255),
    name  varchar(255),
    type  varchar(255)
);

-- solution
CREATE PROCEDURE solution()
SELECT id, login, name
FROM users
WHERE type = 'user'
   OR type != 'user'
ORDER BY id
