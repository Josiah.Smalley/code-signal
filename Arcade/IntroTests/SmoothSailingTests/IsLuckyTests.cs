using Intro.SmoothSailing;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.SmoothSailingTests;

public class IsLuckyTests : BaseTest<IsLucky> {

    [Test]
    public void Test1() { Assert.IsTrue(Solution.solution(1230)); }

    [Test]
    public void Test2() { Assert.IsFalse(Solution.solution(239017)); }

    [Test]
    public void Test3() { Assert.IsTrue(Solution.solution(134008)); }

    [Test]
    public void Test4() { Assert.IsFalse(Solution.solution(10)); }

    [Test]
    public void Test5() { Assert.IsTrue(Solution.solution(11)); }

    [Test]
    public void Test6() { Assert.IsTrue(Solution.solution(1010)); }

    [Test]
    public void Test7() { Assert.IsFalse(Solution.solution(261534)); }

    [Test]
    public void Test8() { Assert.IsFalse(Solution.solution(100000)); }

    [Test]
    public void Test9() { Assert.IsTrue(Solution.solution(999999)); }

    [Test]
    public void Test10() { Assert.IsTrue(Solution.solution(123321)); }

}
