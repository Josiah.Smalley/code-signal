namespace Intro.TheJourneyBegins {
    public class CenturyFromYear {

        public int solution(int year) => (year / 100) + (year % 100 == 0 ? 0 : 1);

    }
}
