using System.Text;
using Common;

namespace Intro.RainbowOfClarity; 

public class LinesEncoding: IHaveSolution<string, string> {


    public string solution(string input) {
        var builder = new StringBuilder();
        var current = input[0];
        var count = 0;

        foreach (var c in input) {
            if (current == c) {
                count++;
                continue;
            }

            if (count > 1) {
                builder.Append(count);
            }

            builder.Append(current);
            current = c;
            count = 1;
        }

        if (count > 1) {
            builder.Append(count);
        }

        builder.Append(current);

        return builder.ToString();
    }

}
