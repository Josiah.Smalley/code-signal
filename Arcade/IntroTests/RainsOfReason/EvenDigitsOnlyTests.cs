using Intro.RainsOfReason;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainsOfReason; 

public class EvenDigitsOnlyTests: BaseTest<EvenDigitsOnly, int, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(int input) => Solution.solution(input);

}
