using Intro.DarkWilderness;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DarkWilderness; 

public class KnapsackLightTests: BaseTest<KnapsackLight, KnapsackLight.Input, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(KnapsackLight.Input input) => Solution.solution(input);

}
