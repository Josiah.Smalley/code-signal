using System;

namespace Intro.EdgeOfTheOcean;

public class MakeArrayConsecutive2
{
    public int solution(int[] statues)
    {
        var min = 21;
        var max = -1;
        foreach (var statue in statues)
        {
            min = Math.Min(min, statue);
            max = Math.Max(max, statue);
        }

        return max - min + 1 - statues.Length;
    }
}