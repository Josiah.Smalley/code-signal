﻿using Common;
using Newtonsoft.Json;
using NUnit.Framework;

namespace TestLibrary {
    public class BaseTest<T>
    where T : new() {

        protected T Solution;

        [SetUp]
        public void Setup() { Solution = new T(); }

        protected TU FromString<TU>(string input) => JsonConvert.DeserializeObject<TU>(input);

    }

    public abstract class BaseTest<TSolution, TIn, TOut> : BaseTest<TSolution>
    where TSolution : new() {

        protected class DataSource : FileInputParameters<TSolution, TIn, TOut> { }

        public abstract TOut TestsFromFile(TIn input);

    }
}
