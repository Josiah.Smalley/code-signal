using Intro.TheJourneyBegins;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.TheJourneyBegins {
    public class AddTests: BaseTest<Add> {

        [Test]
        public void Test1() {
            Assert.AreEqual(3, solution(1, 2));
        }

        [Test]
        public void Test2() {
            Assert.AreEqual(1000, solution(0, 1000));
        }
        
        [Test]
        public void Test3() {
            Assert.AreEqual(-37, solution(2, -39));
        }
        
        [Test]
        public void Test4() {
            Assert.AreEqual(199, solution(99, 100));
        }
        
        [Test]
        public void Test5() {
            Assert.AreEqual(0, solution(-100, 100));
        }
        
        [Test]
        public void Test6() {
            Assert.AreEqual(-2000, solution(-1000, -1000));
        }

        private int solution(int a, int b) => Solution.solution(a, b);

    }
}
