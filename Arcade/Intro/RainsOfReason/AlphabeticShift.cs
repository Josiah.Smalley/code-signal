using System.Linq;
using Common;

namespace Intro.RainsOfReason; 

public class AlphabeticShift: IHaveSolution<string, string> {

    public string solution(string inputString) {
        return string.Join("", inputString.Select(c => c == 'z' ? 'a' : (char)(c + 1)));
    }

}
