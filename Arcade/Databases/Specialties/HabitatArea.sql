-- setup
CREATE TABLE places
(
    x int,
    y int
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Multi AS (
        SELECT CONCAT('MULTIPOINT(',GROUP_CONCAT(CONCAT(x, ' ', y) SEPARATOR ','),')') as mp FROM places
    ),
    GEO AS (SELECT ST_GeomFromText(mp) as g FROM Multi),
    CH AS (SELECT ST_ConvexHull(g) as hull FROM GEO),
    Areas AS (SELECT ST_Area(hull) AS area FROM CH)
    SELECT * FROM Areas;
END