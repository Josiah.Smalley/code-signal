using Intro.TheJourneyBegins;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.TheJourneyBegins {
    public class CenturyFromYearTests: BaseTest<CenturyFromYear> {

        [Test]
        public void Test1() {
            Assert.AreEqual(20, Solution.solution(1905));
        }
        [Test]
        public void Test2() {
            Assert.AreEqual(17, Solution.solution(1700));
        }
        [Test]
        public void Test3() {
            Assert.AreEqual(20, Solution.solution(1988));
        }
        [Test]
        public void Test4() {
            Assert.AreEqual(20, Solution.solution(2000));
        }
        [Test]
        public void Test5() {
            Assert.AreEqual(21, Solution.solution(2001));
        }
        [Test]
        public void Test6() {
            Assert.AreEqual(2, Solution.solution(200));
        }
        [Test]
        public void Test7() {
            Assert.AreEqual(4, Solution.solution(374));
        }
        [Test]
        public void Test8() {
            Assert.AreEqual(1, Solution.solution(45));
        }
        [Test]
        public void Test9() {
            Assert.AreEqual(1, Solution.solution(8));
        }

    }
}
