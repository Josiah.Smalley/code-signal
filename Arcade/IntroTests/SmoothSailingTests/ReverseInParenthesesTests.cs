using Intro.SmoothSailing;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.SmoothSailingTests; 

public class ReverseInParenthesesTests: BaseTest<ReverseInParentheses> {

    [Test]
    public void Test1() {
        var input = "(bar)";
        var expected = "rab";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test2() {
        var input = "foo(bar)baz";
        var expected = "foorabbaz";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test3() {
        var input = "foo(bar)baz(blim)";
        var expected = "foorabbazmilb";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test4() {
        var input = "foo(bar(baz))blim";
        var expected = "foobazrabblim";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test5() {
        var input = "";
        var expected = "";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test6() {
        var input = "()";
        var expected = "";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }
    
    [Test]
    public void Test7() {
        var input = "(abc)d(efg)";
        var expected = "cbadgfe";
        var actual = Solution.solution(input);
        Assert.AreEqual(expected, actual);
    }

}
