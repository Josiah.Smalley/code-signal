using Intro.ThroughTheFog;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.ThroughTheFog;

public class StringRearrangementTests : BaseTest<StringRearrangement, string[], bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(string[] input) => Solution.solution(input);

}
