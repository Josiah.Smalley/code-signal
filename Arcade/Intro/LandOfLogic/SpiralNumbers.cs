using System;
using System.Linq;
using Common;

namespace Intro.LandOfLogic;

public class SpiralNumbers : IHaveSolution<int, int[][]> {

    private enum Direction {

        Right,
        Down,
        Left,
        Up,

    }

    public int[][] solution(int n) {
        var result = Enumerable.Range(0, n).Select(_ => new int[n]).ToArray();

        result[0][0] = 1;

        var value = 2;
        var maxim = n * n;

        var r = 0;
        var c = 0;
        var direction = Direction.Right;

        while (value <= maxim) {
            var nextRow = getNextRow(r, direction);
            var nextCol = getNextCol(c, direction);

            if (!isCellValid(result, nextRow, nextCol)) {
                direction = nextDirection(direction);
                continue;
            }

            r = nextRow;
            c = nextCol;

            result[r][c] = value;
            value += 1;
        }

        return result;
    }

    private Direction nextDirection(Direction current) => (Direction)(((int)current + 1) % 4);

    private bool isCellValid(int[][] matrix, int row, int col) {
        return row >= 0 && row < matrix.Length && col >= 0 && col < matrix[0].Length && matrix[row][col] == 0;
    }

    private int getNextRow(int currentRow, Direction direction) {
        switch (direction) {
            case Direction.Left:
            case Direction.Right:
                return currentRow;

            case Direction.Up:
                return currentRow - 1;

            case Direction.Down:
                return currentRow + 1;

            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }
    }

    private int getNextCol(int currentCol, Direction direction) {
        switch (direction) {
            case Direction.Up:
            case Direction.Down:
                return currentCol;

            case Direction.Left:
                return currentCol - 1;

            case Direction.Right:
                return currentCol + 1;

            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }
    }

}
