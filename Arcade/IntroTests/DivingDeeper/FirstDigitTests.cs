using Intro.DivingDeeper;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.DivingDeeper; 

public class FirstDigitTests: BaseTest<FirstDigit, string, char> {

    [TestCaseSource(typeof(DataSource))]
    public override char TestsFromFile(string input) => Solution.solution(input);

}
