-- setup
CREATE TABLE strs
(
    str VARCHAR(255)
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH RECURSIVE
        Letters AS (SELECT 'a' AS letter
                    UNION ALL
                    SELECT CHAR(ASCII(letter) + 1)
                    FROM Letters
                    WHERE letter < 'z'),
        Occurences AS (SELECT letter,
                              LENGTH(str) - LENGTH(REPLACE(str, letter, '')) AS counts
                       FROM Letters,
                            strs)
    SELECT letter,
           SUM(counts)               AS total,
           SUM(IF(counts > 0, 1, 0)) AS occurrence,
           MAX(counts)               AS max_occurrence,
           SUM(IF(EXISTS(SELECT *
                         FROM Occurences O2
                         WHERE Occurences.counts < O2.counts AND Occurences.letter = O2.letter), 0, 1))
                                     AS max_occurence_reached
    FROM Occurences
    GROUP BY letter
    HAVING total > 0
    ORDER BY letter;
END;