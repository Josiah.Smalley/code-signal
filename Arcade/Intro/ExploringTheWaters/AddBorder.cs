namespace Intro.ExploringTheWaters; 

public class AddBorder {

    public string[] solution(string[] picture) {
        var result = new string[picture.Length + 2];
        var ends = new string('*', picture[0].Length + 2);

        result[0] = ends;
        result[^1] = ends;
        const string format = "*{0}*";
        
        for (var i = 0; i < picture.Length; i++) {
            result[i + 1] = string.Format(format, picture[i]);
        }

        return result;
    }

}
