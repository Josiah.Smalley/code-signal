using System.Collections.Generic;
using System.Text;

namespace Intro.SmoothSailing; 

public class ReverseInParentheses {

    public string solution(string inputString) {
        var stack = new Stack<char>();

        var result = new StringBuilder();
        foreach (var c in inputString)
        {
            switch (c) {
                case '(':
                    stack.Push(c);
                    break;
                case ')':
                    HandleGroup(stack, result);

                    break;
                default:
                    if (stack.Count == 0) {
                        result.Append(c);
                    } else {
                        stack.Push(c);
                    }

                    break;
            }
        }

            return result.ToString();
    }

    private void HandleGroup(Stack<char> stack, StringBuilder result) {
        var queue = new Queue<char>();

        var c = stack.Pop();

        while (c != '(') {
            queue.Enqueue(c);
            c = stack.Pop();
        }

        var toResult = stack.Count == 0;

        while (queue.Count > 0) {
            if (toResult) {
                result.Append(queue.Dequeue());
            } else {
                stack.Push(queue.Dequeue());
            }
        }
    }

}
