namespace Intro.ExploringTheWaters; 

public class AlternatingSums {

    public int[] solution(int[] a) {
        var result = new int[2];

        for (var i = 0; i < a.Length; i++) {
            result[i % 2] += a[i];
        }

        return result;
    }

}
