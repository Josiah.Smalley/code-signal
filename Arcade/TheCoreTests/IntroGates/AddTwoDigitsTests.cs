using TestLibrary;
using TheCore.IntroGates;

namespace TheCoreTests.IntroGates; 

public class AddTwoDigitsTests: BaseTest<AddTwoDigits, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);

}
