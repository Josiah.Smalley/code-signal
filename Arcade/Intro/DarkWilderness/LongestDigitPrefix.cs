using System.Linq;
using Common;

namespace Intro.DarkWilderness; 

public class LongestDigitPrefix: IHaveSolution<string, string> {

    public string solution(string input) => string.Join("", input.TakeWhile(c => c is >= '0' and <= '9'));

}
