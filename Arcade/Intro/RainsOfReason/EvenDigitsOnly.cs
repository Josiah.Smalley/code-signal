using Common;

namespace Intro.RainsOfReason; 

public class EvenDigitsOnly: IHaveSolution<int, bool> {

public bool solution(int n) {
    while (n > 0) {
        if (n % 2 == 1) {
            return false;
        }

        n /= 10;
    }

    return true;
}

}
