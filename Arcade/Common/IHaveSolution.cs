namespace Common {
    public interface IHaveSolution<TIn, TOut> {

        TOut solution(TIn input);

    }
    
}
