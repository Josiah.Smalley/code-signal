using Common;

namespace TheCore.IntroGates; 

public class SeatsInTheater: IHaveSolution<SeatsInTheater.Input, int> {

    public class Input {

        public int nCols { get; set; }
        public int nRows { get; set; }
        public int col { get; set; }
        public int row { get; set; }

    }

    public int solution(Input input) => solution(input.nCols, input.nRows, input.col, input.row);

    public int solution(int nCols, int nRows, int col, int row) => (nRows - row) * (nCols - col + 1);


}
