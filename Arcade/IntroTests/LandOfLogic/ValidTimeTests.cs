using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic; 

public class ValidTimeTests : BaseTest<ValidTime, string, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(string input) => Solution.solution(input);

    

}
