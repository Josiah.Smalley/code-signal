-- setup
CREATE TABLE events
(
    id           INT,
    name         VARCHAR(255),
    event_date   VARCHAR(10),
    participants INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT *
    FROM events
    ORDER BY WEEKDAY(event_date), participants DESC;
END