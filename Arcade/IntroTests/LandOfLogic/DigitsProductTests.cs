using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic;

public class DigitsProductTests : BaseTest<DigitsProduct, int, int> {

    [TestCaseSource(typeof(DataSource))]
    public override int TestsFromFile(int input) => Solution.solution(input);


}
