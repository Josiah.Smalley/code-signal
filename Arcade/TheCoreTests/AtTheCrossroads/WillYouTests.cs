using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class WillYouTests : BaseTest<WillYou, WillYou.WillYouInput, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(WillYou.WillYouInput input) => Solution.solution(input);

    

}
