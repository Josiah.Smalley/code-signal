using Intro.LandOfLogic;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.LandOfLogic; 

public class FileNamingTests : BaseTest<FileNaming, string[], string[]> {

    [TestCaseSource(typeof(DataSource))]
    public override string[] TestsFromFile(string[] input) => Solution.solution(input);

    

}
