namespace Intro.ExploringTheWaters; 

public class AreSimilar {

    public bool solution(int[] a, int[] b) {
        var firstDiff = -1;
        var didSwap = false;
        var length = a.Length;

        for (var i = 0; i < length; i++) {
            if (a[i] == b[i]) {
                continue;
            }

            if (firstDiff == -1) {
                firstDiff = i;
                continue;
            }

            if (didSwap) {
                return false;
            }

            didSwap = true;
            if (a[i] == b[firstDiff] && a[firstDiff] == b[i]) {
                continue;
            }

            return false;
        }

        return firstDiff == -1 || didSwap;

    }


}
