-- setup
CREATE TABLE answers
(
    id             int,
    correct_answer varchar(255),
    given_answer   varchar(255) null
);

-- solution
CREATE PROCEDURE solution()
SELECT id, IF(given_answer is null, 'no answer', IF (given_answer = correct_answer, 'correct', 'incorrect')) AS checks
FROM answers
ORDER BY id;
