using Common;

namespace TheCore.IntroGates; 

public class LargestNumber: IHaveSolution<int, int> {

    public int solution(int input) {
        var result = 0;

        for (var i = 0; i < input; i++) {
            result = (result * 10) + 9;
        }

        return result;
    }

}
