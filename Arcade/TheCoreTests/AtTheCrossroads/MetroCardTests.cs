using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads; 

public class MetroCardTests : BaseTest<MetroCard, int, int[]> {

    [TestCaseSource(typeof(DataSource))]
    public override int[] TestsFromFile(int input) => Solution.solution(input);

    

}
