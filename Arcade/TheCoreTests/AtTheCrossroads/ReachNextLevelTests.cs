using NUnit.Framework;
using TestLibrary;
using TheCore.AtTheCrossroads;

namespace TheCoreTests.AtTheCrossroads;

public class ReachNextLevelTests : BaseTest<ReachNextLevel, ReachNextLevel.ReachNextLevelInput, bool> {

    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(ReachNextLevel.ReachNextLevelInput input) => Solution.solution(input);


}
