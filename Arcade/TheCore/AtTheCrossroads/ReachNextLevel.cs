using Common;

namespace TheCore.AtTheCrossroads;

public class ReachNextLevel : IHaveSolution<ReachNextLevel.ReachNextLevelInput, bool> {

    public class ReachNextLevelInput {

        public int experience { get; set; }
        public int threshold { get; set; }
        public int reward { get; set; }


    }

    public bool solution(ReachNextLevelInput input) => solution(input.experience, input.threshold, input.reward);

    public bool solution(int experience, int threshold, int reward) => threshold <= experience + reward;

}
