using Common;

namespace TheCore.IntroGates;

public class LateRide : IHaveSolution<int, int> {

    public int solution(int input) => DigitSum(input % 60) + DigitSum(input / 60);

    private static int DigitSum(int val) => val < 10 ? val : (val % 10) + DigitSum(val / 10);

}
