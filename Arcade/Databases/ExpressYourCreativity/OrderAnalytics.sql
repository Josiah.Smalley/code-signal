DROP PROCEDURE IF EXISTS solution;
CREATE PROCEDURE solution()
BEGIN

    DROP VIEW IF EXISTS order_analytics;
    CREATE VIEW order_analytics AS
    SELECT id,
           YEAR(order_date) as year,
           QUARTER(order_date) as quarter,
           type,
           price * quantity as total_price
    FROM orders;

    SELECT *
    FROM order_analytics
    ORDER by id;
END;
