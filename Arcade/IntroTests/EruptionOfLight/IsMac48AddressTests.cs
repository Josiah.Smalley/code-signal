using Intro.EruptionOfLight;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.EruptionOfLight;

public class IsMac48AddressTests : BaseTest<IsMac48Address, string, bool> {


    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(string input) => Solution.solution(input);

}
