using Intro.IslandOfKnowledge;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.IslandOfKnowledge; 

public class IsIPv4AddressTests: BaseTest<IsIPv4Address> {

    
    private class DataSource : FileInputParameters<string, bool> {
    
        protected override string FileName => "IsIPv4Address";
    
    }
    
    [TestCaseSource(typeof(DataSource))]
    public bool TestsFromFile(string input) => Solution.solution(input);

}
