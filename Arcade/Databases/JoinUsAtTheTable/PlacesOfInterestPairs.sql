-- setup
CREATE TABLE sights
(
    id   INT,
    name VARCHAR(255),
    x    FLOAT,
    y    FLOAT
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    SELECT S1.name AS place1,
           S2.name AS place2
    FROM sights S1,
         sights S2
    WHERE s1.name < s2.name
      AND POW(s1.x - s2.x, 2) + POW(s1.y - s2.y, 2) < 25
    ORDER BY place1, place2;
END