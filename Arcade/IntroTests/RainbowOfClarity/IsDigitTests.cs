using Intro.RainbowOfClarity;
using NUnit.Framework;
using TestLibrary;

namespace IntroTests.RainbowOfClarity;

public class IsDigitTests : BaseTest<IsDigit, char, bool> {


    [TestCaseSource(typeof(DataSource))]
    public override bool TestsFromFile(char input) => Solution.solution(input);

}
