-- setup
CREATE TABLE Department
(
    id   INT,
    name VARCHAR(255)
);

CREATE TABLE Employee
(
    id         INT,
    full_name  VARCHAR(255),
    department INT,
    salary     INT
);

-- solution
CREATE PROCEDURE solution()
BEGIN

    WITH Filtered AS (SELECT name                     AS dep_name,
                             Department.id            AS dep_id,
                             COUNT(full_name)         AS emp_number,
                             COALESCE(SUM(salary), 0) AS total_salary
                      FROM Department
                               LEFT JOIN Employee ON department = Department.id
                      GROUP BY dep_id, dep_name
                      HAVING emp_number < 6),
         Ordered AS (SELECT Filtered.*, (@rowIdx := @rowIdx + 1) AS idx
                     FROM Filtered,
                          (SELECT @rowIdx := 0) AS placeholder
                     ORDER BY total_salary DESC, emp_number DESC, dep_id)
    SELECT dep_name, emp_number, total_salary
    FROM Ordered
    WHERE idx MOD 2 = 1;
END;