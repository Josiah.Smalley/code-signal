using System;
using System.Collections.Generic;
using System.Linq;

namespace Intro.SmoothSailing;

public class CommonCharacterCount {

    public int solution(string s1, string s2) {
        var firstChars = GetCharCounts(s1);
        var secondChars = GetCharCounts(s2);

        return firstChars.Select(pair => s2.Contains(pair.Key) ? Math.Min(pair.Value, secondChars[pair.Key]) : 0).Sum();
    }

    private Dictionary<char, int> GetCharCounts(string s) {
        return s.GroupBy(c => c).ToDictionary(group => group.Key, group => group.Count());
    }

}
