using Common;

namespace TheCore.AtTheCrossroads;

public class TennisSet : IHaveSolution<TennisSet.TennisSetInput, bool> {

    public class TennisSetInput {

        public int score1 { get; set; }
        public int score2 { get; set; }

    }

    public bool solution(TennisSetInput input) => solution(input.score1, input.score2);

    public bool solution(int score1, int score2) {
        var min = Math.Min(score1, score2);
        var max = Math.Max(score1, score2);

        return (max == 6 && min < 5) || (max == 7 && (min > 4 && min < max));

    }

}
