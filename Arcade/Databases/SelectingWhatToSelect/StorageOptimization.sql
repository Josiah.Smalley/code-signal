-- setup
CREATE TABLE workers_info
(
    id            INT,
    name          VARCHAR(255) NULL,
    date_of_birth DATE         NULL,
    salary        INT          NULL
);

-- solution
CREATE PROCEDURE solution()
BEGIN
    WITH Names AS (SELECT id,
                          'name' AS column_name,
                          name   AS value,
                          0      AS sort_order
                   FROM workers_info
                   WHERE name IS NOT NULL),
         Birthdays AS (SELECT id,
                              'date_of_birth'                        AS column_name,
                              DATE_FORMAT(date_of_birth, '%Y-%m-%d') AS value,
                              1                                      AS sort_order
                       FROM workers_info
                       WHERE date_of_birth IS NOT NULL),
         Salaries AS (SELECT id,
                             'salary' AS column_name,
                             salary   AS value,
                             2        AS sort_order
                      FROM workers_info
                      WHERE salary IS NOT NULL)
    SELECT id, column_name, value
    FROM (SELECT *
          FROM Names
          UNION
          SELECT *
          FROM Birthdays
          UNION
          SELECT *
          FROM Salaries) U1
    ORDER BY id, sort_order;
END;