namespace Intro.TheJourneyBegins {
    public class Add {

        public int solution(int a, int b) => a + b;

    }
}
