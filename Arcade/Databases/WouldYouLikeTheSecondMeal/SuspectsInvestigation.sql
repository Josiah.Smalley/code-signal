-- Setup

CREATE TABLE Suspect
(
    id      int,
    name    varchar(255),
    surname varchar(255),
    height  int,
    weight  int
);


-- Solution

CREATE PROCEDURE solution()
BEGIN
    SELECT id, name, surname
    FROM Suspect
    WHERE height <= 170
      AND name LIKE 'B%'
      AND surname LIKE 'Gre_n'
    ORDER BY id;

END;