using System.Linq;
using Common;

namespace Intro.EruptionOfLight;

public class BuildPalindrome : IHaveSolution<string, string> {

    public string solution(string st) {
        var i = 0;

        while (!isPalindrome(st, i, st.Length - 1)) {
            i += 1;
        }

        var prefix = st[..i];

        return st + new string(prefix.Reverse().ToArray());
    }

    private static bool isPalindrome(string st, int start, int end) {
        while (true) {
            if (start > end) {
                return true;
            }

            if (st[start] != st[end]) {
                return false;
            }

            start += 1;
            end -= 1;
        }
    }

}
