using System.Text.RegularExpressions;
using Common;

namespace Intro.LandOfLogic; 

public class ValidTime: IHaveSolution<string, bool> {

    public bool solution(string input) => Regex.IsMatch(
        input,
        @"^([01][0-9]|2[0-3]):([0-5][0-9])$");

}
